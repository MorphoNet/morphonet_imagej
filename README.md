# MorphoNetJ Plugin

This plugin is used to upload data to MorphoNet using [ImageJ](https://imagej.nih.gov/ij/) or [Fiji](https://fiji.sc/)

This plugin uses segmented 3D or 4D images as input, and allows you to create meshes, that can be uploaded to MorphoNet for viewing (among other functionalities).

You can find help and tutorials in the help section of [MorphoNet](https://morphonet.org/help_fiji).

We highly recommend the use of the last Fiji version that includes automatically the last 3D library [https://fiji.sc](https://fiji.sc/#download)

## Description

- The MorphoNetJ folder contains the Eclipse maven project with the plugin.

- The macOS, Ubuntu and windows folders contain the libraries necessary to install the plugin on Fiji.

- The .md Folders contain the documentation concerning the plugin usage

- The MorphoNet_-\*.\*.\*.jar plugin is the last version of the plugin

- The vtk.jar library is a library necessary to compile the MorphoNetJ plugin in Eclipse.


## Installation

Please refer to the INSTALL.md file for information on how to install the plugin.

## How to use

Please refer to the TUTORIAL.md and ADVANCED_TUTORIAL.md files for information on how to use the plugin.


## Compile The plugin on your computer

- Open Eclise (http://eclipse.org)
- File > Import > Maven > Existing Maven project
- Choose the MorphoNetJ path, and check the project with the .pom file
- Then you need to install the vtk.jar library in your personal maven dependencies directory.
  - To do this, run this : *mvn install:install-file "-Dfile=full\\path\\to\\the\\vtk.jar" "-DgroupId=com.vtk" "-DartifactId=vtk" "-Dversion=1.0" "-Dpackaging=jar" "-DgeneratePom=true"*
- Configure Build Path
- Add External Jar
- Chose in the plugin Path the 3D_Viewer-4.0.2.jar file
- And compile your plugin wth maven. By default, the build is in the *target* directory
- make sure to follow the install instruction in INSTALL.md if you want to try the plugin in FIJI


## Authors


Emmanuel Faure  [1,2]

Benjamin Gallean [1,2]

Tao Laurent [2]


with theses affiliations

[1] Centre de Recherche de Biologie cellulaire de Montpellier, CRBM, CNRS, Université de Montpellier, France.
[2] Laboratoire d'Informatique, de Robotique et de Microélectronique de Montpellier (LIRMM), LIRMM, Univ Montpellier, CNRS, Montpellier, France.

*Correspondence should be addressed to Emmanuel Faure (emmanuel.faure@lirmm.fr)

## License
This project is licensed under the CeCILL License - see the LICENSE.md file for details
