# MorphoNetJ Plugin help

This plugin requires the last version of [Fiji](https://imagej.net/software/fiji/) to function properly.

This plugin allows users to convert segmented 3D or 4D images into meshes, and upload these meshes to datasets on morphonet for viewing.

![](helpfiles/images/fiji/PluginExplain.png)

## Downloads

- Windows 10 : [Download Plugin](helpfiles/FijiPlugins/MorphoNetJ_WINDOWS.zip)

- macOS : [Download Plugin](helpfiles/FijiPlugins/MorphoNetJ_macOS.zip)

- Ubuntu: [Download Plugin](helpfiles/FijiPlugins/MorphoNetJ_Ubuntu.zip)

- Jar plugin (without librairies for quick update) : [Download Plugin](helpfiles/FijiPlugins/MorphoNet_-0.1.2.jar)

## Bug reports / feedback

This plugin is still in development; if you find any bugs, or have some recommendations, please submit these using the Feedback panel or the corresponding slack channel.


## Installing the plugin



### macOS

- First of all, decompress the dowloaded plugin.

- Then, drag the plugin "MorphoNet_-\*.\*.\*.jar" in your Fiji.app/plugins plugins folder.

- Finally, move the MorphoNetLibs folder of the plugin into the Fiji.app/lib folder of your Fiji install.

### Windows 10/11 :

- First of all, decompress the dowloaded plugin.

- Then, drag the plugin "MorphoNet_-\*.\*.\*.jar" in your Fiji.app/plugins plugins folder.

 - Drag and drop the MorphoNetLibs folder to the "Fiji.app/lib/" folder of your local Fiji install.

### Ubuntu
- First of all, decompress the dowloaded plugin.

- Then, drag the plugin "MorphoNet_-\*.\*.\*.jar" in your Fiji.app/plugins plugins folder.

 - Finally, Copy the MorphoNetLibs folder of the plugin into the Fiji.app/lib folder of your Fiji install.
Also, make sure you have the openjdk-8-jdk installed, as well as openGL3.x. You can install these by running the attached "linux_INSTALLER.sh" file (command "bash linux_INSTALLER.sh")

### Updating the plugin

 - Just replace the MorphoNet jar Plugin in your Fiji Plugins folder with the newly downloaded one.
 - If you want to get automatic updates using the Fiji updates system, please refer to the *Enable automatic updates* section at the very end of this tutorial.


## Enable automatic updates

Once you installed the required libraries, you can enable the automatic downloading of updates on Fiji to get the latest updates. To do so, click on the Help > Update... button.

![](helpfiles/images/fiji/update1.png)

On the next menu, click on the *Manage updates sites* button. On the new window, click on the Add update site button and add a website with the name "MorphoNet", with the URL "https://sites.imagej.net/MorphoNet/".Make sure the site is checked, then click on the *Close* button.

![](helpfiles/images/fiji/update2.png)

Finally, close Fiji, re-open it, and click on the Help > Update... Button once more to get the updated plugin. Make sure the MorphoNet plugin in on the Install/Update option, then click on the *Apply changes* button, and you are set ! When you want to update the plugin, you just need to click on the Help > Update... button and accept the update.

![](helpfiles/images/fiji/update3.png)
