package com.MorphoNet.imagej;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.font.TextAttribute;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;


import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.json.JSONArray;
import org.json.JSONObject;
import org.scijava.vecmath.Point3f;

import customnode.CustomMesh;
import customnode.CustomTriangleMesh;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.Macro;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.macro.MacroRunner;
import ij.measure.ResultsTable;
import ij.plugin.ImageJ_Updater;
import ij.plugin.PlugIn;
import ij.plugin.Scaler;
import ij.process.ByteProcessor;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;
import ij3d.Image3DUniverse;
import vtk.vtkDataObject;
import vtk.vtkDecimatePro;
import vtk.vtkDiscreteMarchingCubes;
import vtk.vtkImageCanvasSource2D;
import vtk.vtkImageData;
import vtk.vtkImageImport;
import vtk.vtkImageReader2;
import vtk.vtkImageReader2Factory;
import vtk.vtkNativeLibrary;
import vtk.vtkPolyData;
import vtk.vtkQuadricClustering;
import vtk.vtkWindowedSincPolyDataFilter;

public class ConvertImageToMesh implements PlugIn{

	//connection parameters
	private static String login;
	private static String passwd;
	private static String token;
	//private static int user_id;
	private static boolean connected=false;
	private static String morphonet_URL="https://morphonet.org/";

	//UI_related
	private static Frame mm;
	private static Frame pm;
	private static Button convertBtn;
	private static TextField vXvalue;
	private static TextField vYvalue;
	private static TextField vZvalue;
	private static Choice imgChoice;
	private static Label convertImgErrorLb;
	private static Label convertPathErrorLb;
	private static boolean missingImg=true;
	private static boolean missingPath=true;
	private static boolean missingObjs=true;
	private static boolean missingDataset=true;
	private static Label uploadObjErrorLb;
	private static Label uploadDatasetErrorLb;
	private static JFileChooser fileChooser;
	private static Label foundMeshesLabel;
	private static String defaultObjPath="";
	private static String defaultDirPath="";
	private static Label minTimeStepLb;
	private static Label maxTimeStepLb;
	private static TextField minTSTF;
	private static TextField maxTSTF;
	private static Choice fileToUploadChoice;
	private static TextField pathToUploadTF;
	private static Label filesUploadReminder;
	private static TextField datasetToUpdate;
	private static Label uploadTSLb;
	private static TextField uploadTSTF;
	private static TextField filePathTF;
	private static Button createNewDataset;
	private static Button uploadToMNBtn;
	private static Panel advancedPanel;
	private static Panel uploadAdvancedPanel;
	private static Checkbox centerB;
	private static Checkbox clearBeforeUpB;
	private static TextField channelTF;
	private static JSlider qualityTF;
	
	/*--------UI_GENERATORS-------------*/
	private static Font mainPartFont;
	private static Panel convertPanel;
	private static Panel MeshPanel;
	private static Panel uploadPanel;
	private static JSlider factorScaleSlider;
	private static JSlider ZfactorScaleSlider;
	private static TextField bkgVal;
	private static Checkbox centerizeCb;
	private static TextField minVoxVal;
	private static Checkbox borderBox;
	private static Checkbox smoothBox;
	private static TextField spVal;
	private static TextField siVal;
	private static Checkbox QCBox;
	private static TextField ndVal;
	private static Checkbox DBox;
	private static TextField drVal;
	private static Choice verboseValue;
	private static Checkbox ViewerBox;
	private static Checkbox datasetClear;
	private static Button seeDatasetInMN;
	private static Label numberOfFilesLb;
	private static Choice multiThreadVal;
	private static TextField autoDecimateThresholdTF;
	private static TextField splitMeshThreshold;
	private static Checkbox splitMeshActivate;
	
	/*------IMAGES MENU----------*/
	private static Panel imagesPanel;
	private static Label PickDatasetField;
	private static Label ImagesImgErrorLb;
	private static Choice ImageImgChoice;
	private static Label imgMinTimeStepLb;
	private static TextField imgMinTSTF;
	private static Label imgMaxTimeStepLb;
	private static TextField imgMaxTSTF;
	private static Label datasetMissingLb;
	private static Button UploadImageToMN;
	private static JSlider rawImgScaleSlider;
	
	/*------INFOS MENU------------*/
	private static Panel infosPanel;
	private static Button UploadFromFiji;
	private static Label featuresMissingWarningLb;
	private static Button UploadFromFile;
	private static TextField featureFilePath;
	
	private static JProgressBar ConvertProgress;
	private static JProgressBar UploadProgress;
	private static JProgressBar ImageProgress;

	private static boolean create_dataset=true;
	private static int dataset_update_id=-1;
	private static int startTS=1;
	private static int endTS=-1;

	private static boolean launchConvert = false;

	private static Map<String,Integer> userDatasetsNames;

	private static int VTK_UNSIGNED_CHAR = 3;
	public static boolean loaded=true;
	private static int MAXOBJ=10000;

	private static int startF=1; 
	private static int endF=1; 

	static SSLContext sc;
	
	private static String LocalMNPath = "";
	
	private static boolean replaceOff=false;
	
	private static String currentVersion; 
	private static boolean upToDate = true;
	
	//initialize parameters and load libraries
	static
    {
		Properties systemProps = System.getProperties();
	    systemProps.put("javax.net.ssl.trustStore", "clienttruststore.jks");
	    systemProps.put("javax.net.ssl.trustStorePassword","password");
	    System.setProperties(systemProps);	
		
		
		if(System.getProperty("os.name").toLowerCase().contains("mac")) {//macOS case
			System.out.println("loading libraries...");
			String osMorphoDir;
            if(new File(File.separator+"Applications"+File.separator+"Fiji.app").exists()) {//fiji
            	System.out.println("FIJI install detected");
                osMorphoDir = File.separator+"Applications"+File.separator+"Fiji.app"+File.separator+"lib"+File.separator+"MorphoNetLibs"+File.separator;
            }else {//imageJ
                osMorphoDir = File.separator+"Applications"+File.separator+"ImageJ.app"+File.separator+"lib"+File.separator+"MorphoNetLibs"+File.separator;
            }
			boolean load=true;
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				boolean t=true;
				try {
					System.load(osMorphoDir+"lib"+lib.GetLibraryName()+".jnilib");
				}catch(UnsatisfiedLinkError e) {
					System.out.println(lib.GetLibraryName()+" was not loaded");
					t=false;
					load=false;
				}

			}
			System.out.println("libraries finished loading !");
		}else if(System.getProperty("os.name").toLowerCase().contains("linux")){//linux/Ubuntu case
			System.out.println("loading libraries...");
			String osMorphoDir;
            osMorphoDir = System.getProperty("user.dir")+File.separator+"lib"+File.separator+"MorphoNetLibs"+File.separator;
			boolean load=true;
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				boolean t=true;
				try {
					System.load(osMorphoDir+"lib"+lib.GetLibraryName()+".so");
				}catch(UnsatisfiedLinkError e) {
					System.out.println(lib.GetLibraryName()+" was not loaded "+e);
					t=false;
					load=false;
				}

			}
			System.out.println("libraries finished loading !");
		}else{//Windows 10 case
			
			System.out.println("loading libraries...");
			String osMorphoDir;
            osMorphoDir = System.getProperty("user.dir")+File.separator+"lib"+File.separator+"MorphoNetLibs"+File.separator;
			boolean load=true;
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				boolean t=true;
				try {
					System.load(osMorphoDir+lib.GetLibraryName()+".dll");
				}catch(UnsatisfiedLinkError e) {
					System.out.println(lib.GetLibraryName()+" was not loaded "+e);
					t=false;
					load=false;
				}

			}
			System.out.println("libraries finished loading !");
			
		}
		
		//init local morphonet folder on desktop for temp files
		LocalMNPath = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString();
		if(System.getProperty("os.name").toLowerCase().contains("mac")) {//change the path if we are on mac
			LocalMNPath = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop"+File.separator+"MorphoNet"+File.separator;
		}
		if(System.getProperty("os.name").toLowerCase().contains("linux")) {//change the path if we are on linux
			LocalMNPath = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop"+File.separator+"MorphoNet"+File.separator;
		}

    }
	
	public static Image getImage(final String pathAndFileName) {
	    final URL url = Thread.currentThread().getContextClassLoader().getResource(pathAndFileName);
	    return Toolkit.getDefaultToolkit().getImage(url);
	}
	
	
	// always verify the host - dont check for certificate
	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
	    public boolean verify(String hostname, SSLSession session) {
	        return true;
	    }
	};

	/**
	 * Trust every server - dont check for any certificate
	 */
	private static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
	    TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
	        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	            return new java.security.cert.X509Certificate[] {};
	        }

	        public void checkClientTrusted(X509Certificate[] chain,
	                String authType) throws CertificateException {
	        }
	        
	        public void checkServerTrusted(X509Certificate[] chain,
	                String authType) throws CertificateException {
	        }

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws java.security.cert.CertificateException {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws java.security.cert.CertificateException {
			}
	    } };

	    
	    // Install the all-trusting trust manager
	    try {
	        SSLContext sc = SSLContext.getInstance("TLS");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection
	                .setDefaultSSLSocketFactory(sc.getSocketFactory());
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

	/**
	 * keyAdapter to prevent non-numeric characters in number text fields (floats)
	 */
	public static KeyAdapter kNumber = new KeyAdapter() {
		public void keyPressed(KeyEvent ke) {
			try {
				TextField tf = (TextField)ke.getSource();
				String value = tf.getText();
				int l = value.length();
	            if((ke.getKeyChar()>='0' && ke.getKeyChar()<='9') || ke.getKeyChar()=='.'  || ke.getKeyCode()==KeyEvent.VK_BACK_SPACE) {
	            	tf.setEditable(true);
	            }else {
	            	tf.setEditable(false);
	            	tf.setBackground(new Color(1.f,1.f,1.f));
	            }
			}catch(Exception e){
				System.out.println("ERROR, object "+ke.getSource().toString()+" could not be interpreted as a textfield. Error : "+e);
			}
            
         }
	};
	
	/**
	 * keyAdapter to prevent non-numeric characters in number text fields (integers)
	 */
		public static KeyAdapter kNumberInt = new KeyAdapter() {
			public void keyPressed(KeyEvent ke) {
				try {
					TextField tf = (TextField)ke.getSource();
					String value = tf.getText();
					int l = value.length();
		            if((ke.getKeyChar()>='0' && ke.getKeyChar()<='9') || ke.getKeyCode()==KeyEvent.VK_BACK_SPACE) {
		            	tf.setEditable(true);
		            }else {
		            	tf.setEditable(false);
		            	tf.setBackground(new Color(1.f,1.f,1.f));
		            }
				}catch(Exception e){
					System.out.println("ERROR, object "+ke.getSource().toString()+" could not be interpreted as a textfield. Error : "+e);
				}
	            
	         }
		};

	/**
	 * Get current up-to-date version on the server. Returns a string in format "x.y.z"
	 */
	public static String GetCurrentUpToDateVersion()  {
		try {
			URL url = new URL("https://sites.imagej.net/MorphoNet/plugins");

			HttpsURLConnection connec = (HttpsURLConnection)url.openConnection();
			connec.setHostnameVerifier(DO_NOT_VERIFY);
			connec.setRequestMethod("GET");

			connec.setDoOutput(true);


			Reader in = new BufferedReader(new InputStreamReader(connec.getInputStream(), "UTF-8"));
	        String output="";
	        
	        for (int c; (c = in.read()) >= 0;)
	        	output+=(char)c;

	        //will it always be the last ?
	        String ver = output.split("MorphoNet_-")[output.split("MorphoNet_-").length-1];
	        String versionNb = ver.split(".jar")[0];
			
			return versionNb;
		}catch(Exception e) {
			System.out.println("Could not get last plugin version : "+e);
			return "";
		}
		
	}
	

		
	/**
	 * Main function, launched on first start of the plugin. opens the main menu and checks current version of the plugin
	 */
	@Override
	public void run(String arg) {
		
		//check if up-to date : 
		Package mainPackage = this.getClass().getPackage();
		currentVersion = mainPackage.getImplementationVersion();
		
		
		
		
		if(arg.equals("MorphoNetMainMenu")) {
			PluginMenu();
		}

	}
	
	/**UI GENERATORS**/
	/**
	 * UI building function to avoid copying code (all elements need to be static) - builds convert panel + advanced options
	 * @param heightStart height at which the menu is initialized, as per java awt specifications (e.g : setBounds parameters)
	 * @param uploadMenu determines if the upload menu will be present in this instance of the plugin.
	 */
	public static void BuildConvertMenu(int heightStart, boolean uploadMenu) {
		/**Convert part**/
		
		convertPanel = new Panel();
		convertPanel.setBackground(new Color(0.9f,0.9f,0.9f));
		convertPanel.setBounds(0, heightStart, 600, 220);
		convertPanel.setLayout(null);
		mm.add(convertPanel);
		
		Label convertLb = new Label("Convert Segmented image to .obj mesh");
		convertLb.setBackground(new Color(0.9f,0.9f,0.9f));
		convertLb.setFont(mainPartFont);
		convertLb.setBounds(50, 0, 350, 30);
		convertPanel.add(convertLb);



		//convert error message label
		convertImgErrorLb = new Label("Needs an image!");
		convertImgErrorLb.setForeground(Color.RED);
		convertImgErrorLb.setBackground(new Color(0.9f,0.9f,0.9f));
		convertImgErrorLb.setBounds(130, 72, 130, 18);
		convertPanel.add(convertImgErrorLb);

		convertPathErrorLb = new Label("Needs mesh path!", Label.CENTER);
		convertPathErrorLb.setForeground(Color.RED);
		convertPathErrorLb.setBackground(new Color(0.9f,0.9f,0.9f));
		convertPathErrorLb.setBounds(450, 180, 130, 20);
		convertPanel.add(convertPathErrorLb);
		convertPathErrorLb.setVisible(false);


		//image choice
		Label fileChoiceLb = new Label("Input image");
		fileChoiceLb.setBackground(new Color(0.9f,0.9f,0.9f));
		fileChoiceLb.setBounds(50,50,75,20);
		convertPanel.add(fileChoiceLb);

		imgChoice = new Choice();
		for(String i : WindowManager.getImageTitles()) {
			imgChoice.add(i);
		}

		imgChoice.setBounds(130, 50, 250, 20);
		convertPanel.add(imgChoice);

		//update error message if no images !
		if(WindowManager.getImageTitles().length==0) {
			missingImg=true;
		}else {
			missingImg=false;
			convertImgErrorLb.setVisible(false);
		}


		ImagePlus curr;
		float[] voxel_sizes = {1.f,1.f,1.f};
		int def_bkg=0;
		if((curr = WindowManager.getImage(imgChoice.getSelectedItem()))!=null) {
			voxel_sizes[0] = (float) curr.getCalibration().pixelWidth;
			voxel_sizes[1] = (float) curr.getCalibration().pixelHeight;
			voxel_sizes[2] = (float) curr.getCalibration().pixelDepth;
			
			//update background value
			def_bkg = GetLowestValueFromImage(curr);

		}
		imgChoice.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) { //on changing item, update voxel_size
				ImagePlus curr;
				if((curr = WindowManager.getImage(e.getItem().toString()))!=null) {
					voxel_sizes[0] = (float) curr.getCalibration().pixelWidth;
					voxel_sizes[1] = (float) curr.getCalibration().pixelHeight;
					voxel_sizes[2] = (float) curr.getCalibration().pixelDepth;
					vXvalue.setText(Float.toString(voxel_sizes[0]));
					vYvalue.setText(Float.toString(voxel_sizes[1]));
					vZvalue.setText(Float.toString(voxel_sizes[2]));
					
					//update background value
					int defbkg = GetLowestValueFromImage(curr);
					bkgVal.setText(String.valueOf(defbkg));
					
					if(uploadMenu) {
						uploadDatasetErrorLb.setVisible(false);
					}
					
					if(convertBtn!=null && datasetToUpdate!=null) {
						if(datasetToUpdate.getText()!=null) {
							convertBtn.setEnabled(true);
						}
					}
					
				}

				if(datasetToUpdate!=null) {
					if(imgChoice.getSelectedItem()!=null) {
						datasetToUpdate.setText(removeExtensionFromFile(e.getItem().toString()));
					}
				}
				//update error messages and update dataset_upload_id to upload dataset
				if(uploadMenu) {
					if(userDatasetsNames.containsKey(datasetToUpdate.getText().toLowerCase())) {//if dataset already exists, put a warning label!
						uploadDatasetErrorLb.setVisible(true);
						uploadDatasetErrorLb.setText("Dataset already exists: check option below to replace time steps");
						datasetClear.setVisible(true);
						datasetClear.setState(true);
						create_dataset=false;
						dataset_update_id=userDatasetsNames.get(datasetToUpdate.getText().toLowerCase());
					}else {
						create_dataset=true;
						dataset_update_id=-1;
						uploadDatasetErrorLb.setVisible(false);
						datasetClear.setVisible(false);
						datasetClear.setState(false);
					}
				}
				
				//also change default filechooser file value to mach selected file
				String defaultObjPath = imgChoice.getSelectedItem().split("\\.")[0]+".obj";
				fileChooser.setSelectedFile(new File(defaultDirPath+defaultObjPath));
				
				filePathTF.setText(defaultDirPath+defaultObjPath);
				
				if(ConfirmIfObjAlreadyExistsInPath(filePathTF.getText(),getFileFromPath(filePathTF.getText()))) {
					foundMeshesLabel.setVisible(true);
				}else {
					foundMeshesLabel.setVisible(false);
				}
				
				//also update time step GUI if stack or hyperstack
				//if hyperstack:
				if(WindowManager.getImage(imgChoice.getSelectedItem()).getNFrames()>1) {
					minTimeStepLb.setText("Min step");
					maxTimeStepLb.setVisible(true);

					maxTSTF.setText(String.valueOf(WindowManager.getImage(imgChoice.getSelectedItem()).getNFrames()-1));
					maxTSTF.setVisible(true);
				}else {
					minTimeStepLb.setText("Time step");
					minTSTF.setText("0");
					maxTSTF.setVisible(false);
					maxTSTF.setText("-1");
					maxTimeStepLb.setVisible(false);
				}
				

			}

		});


		//factor scale :
		Label factorScaleLb = new Label("Downscale");
		factorScaleLb.setBackground(new Color(0.9f,0.9f,0.9f));
		factorScaleLb.setBounds(50,145,75,20);
		convertPanel.add(factorScaleLb);
		
		Label factorScaleXYLb = new Label("XY");
		factorScaleXYLb.setBackground(new Color(0.9f,0.9f,0.9f));
		factorScaleXYLb.setBounds(130,145,30,20);
		convertPanel.add(factorScaleXYLb);

		factorScaleSlider = new JSlider(JSlider.HORIZONTAL,1,4,4);
		factorScaleSlider.setBounds(170, 125, 120, 60);
		factorScaleSlider.setBackground(new Color(0.9f,0.9f,0.9f));
		factorScaleSlider.setMajorTickSpacing(1);
		factorScaleSlider.setPaintLabels(true);
		factorScaleSlider.setPaintTicks(true);
		convertPanel.add(factorScaleSlider);
		
		Label factorScaleZLb = new Label("Z");
		factorScaleZLb.setBackground(new Color(0.9f,0.9f,0.9f));
		factorScaleZLb.setBounds(300,145,30,20);
		convertPanel.add(factorScaleZLb);
		
		ZfactorScaleSlider = new JSlider(JSlider.HORIZONTAL,1,4,4);
		ZfactorScaleSlider.setBounds(340, 125, 120, 60);
		ZfactorScaleSlider.setBackground(new Color(0.9f,0.9f,0.9f));
		ZfactorScaleSlider.setMajorTickSpacing(1);
		ZfactorScaleSlider.setPaintLabels(true);
		ZfactorScaleSlider.setPaintTicks(true);
		convertPanel.add(ZfactorScaleSlider);

		factorScaleSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				ZfactorScaleSlider.setValue(factorScaleSlider.getValue());
		      }
		});
		

		/**advanced options : they must be initialized before the button**/

		advancedPanel = new Panel();
		advancedPanel.setBackground(new Color(0.8f,0.8f,0.8f));
		advancedPanel.setBounds(600, 50, 380, 540);
		advancedPanel.setLayout(null);

		
		//Voxel_size
		Label voxelSizeLb = new Label("Voxel size");
		voxelSizeLb.setBackground(new Color(0.8f,0.8f,0.8f));
		voxelSizeLb.setBounds(10, 10, 75, 30);
		advancedPanel.add(voxelSizeLb);

		vXvalue = new TextField();
		vXvalue.setText(Float.toString(voxel_sizes[0]));
		vXvalue.setBounds(90, 14, 90, 22);
		vXvalue.addKeyListener(kNumber);
		advancedPanel.add(vXvalue);
		vYvalue = new TextField();
		vYvalue.setText(Float.toString(voxel_sizes[1]));
		vYvalue.setBounds(185, 14, 90, 22);
		vYvalue.addKeyListener(kNumber);
		advancedPanel.add(vYvalue);
		vZvalue = new TextField();
		vZvalue.setText(Float.toString(voxel_sizes[2]));
		vZvalue.setBounds(280, 14, 90, 22);
		vZvalue.addKeyListener(kNumber);
		advancedPanel.add(vZvalue);
		

		//min_voxel
		Label minVoxelLb = new Label("Min voxel");
		minVoxelLb.setBackground(new Color(0.8f,0.8f,0.8f));
		minVoxelLb.setBounds(10, 40, 75, 30);
		advancedPanel.add(minVoxelLb);

		minVoxVal = new TextField();
		minVoxVal.setText("10");
		minVoxVal.setBounds(90, 44, 30, 22);
		minVoxVal.addKeyListener(kNumberInt);
		advancedPanel.add(minVoxVal);

		//close border
		Label closeBorderLb = new Label("Close border");
		closeBorderLb.setBackground(new Color(0.8f,0.8f,0.8f));
		closeBorderLb.setBounds(10, 70, 80, 30);
		advancedPanel.add(closeBorderLb);

		borderBox = new Checkbox("",true);
		borderBox.setBounds(100,70,30,30);
		advancedPanel.add(borderBox);


		Font advancedPartsFont = new Font("Verdana", Font.PLAIN,13);

		/**smooth_panel**/
		Label smoothingPartLb = new Label("Smoothing");
		smoothingPartLb.setFont(advancedPartsFont);
		smoothingPartLb.setBackground(new Color(0.8f,0.8f,0.8f));
		smoothingPartLb.setBounds(10, 100, 100, 30);
		advancedPanel.add(smoothingPartLb);
		//smoothing
		smoothBox = new Checkbox("",true);
		smoothBox.setBounds(110,100,30,30);
		advancedPanel.add(smoothBox);

		//smooth_passband
		Label smoothPassbandLb = new Label("Smooth passband");
		smoothPassbandLb.setBackground(new Color(0.8f,0.8f,0.8f));
		smoothPassbandLb.setBounds(20, 130, 130, 30);
		advancedPanel.add(smoothPassbandLb);

		spVal = new TextField();
		spVal.setText("0.01");
		spVal.setBounds(200, 134, 40, 22);
		spVal.addKeyListener(kNumber);
		advancedPanel.add(spVal);

		//smooth_iterations
		Label smoothIterationsLb = new Label("Smooth iterations");
		smoothIterationsLb.setBackground(new Color(0.8f,0.8f,0.8f));
		smoothIterationsLb.setBounds(20, 160, 130, 30);
		advancedPanel.add(smoothIterationsLb);

		siVal = new TextField();
		siVal.setText("25");
		siVal.setBounds(200, 164, 40, 22);
		siVal.addKeyListener(kNumberInt);
		advancedPanel.add(siVal);

		/**Quadric_clustering part**/
		Label QCPartLb = new Label("Quadric clustering");
		QCPartLb.setFont(advancedPartsFont);
		QCPartLb.setBackground(new Color(0.8f,0.8f,0.8f));
		QCPartLb.setBounds(10, 190, 150, 30);
		advancedPanel.add(QCPartLb);
		//QC
		QCBox = new Checkbox("",true);
		QCBox.setBounds(160,190,30,30);
		advancedPanel.add(QCBox);

		//number of divisions
		Label nbDivLb = new Label("Number of divisions");
		nbDivLb.setBackground(new Color(0.8f,0.8f,0.8f));
		nbDivLb.setBounds(20, 220, 130, 30);
		advancedPanel.add(nbDivLb);

		ndVal = new TextField();
		ndVal.setText("1");
		ndVal.addKeyListener(kNumberInt);
		ndVal.setBounds(200, 224, 40, 22);
		advancedPanel.add(ndVal);

		/**Decimation part**/
		Label decimationPartLb = new Label("Decimation");
		decimationPartLb.setFont(advancedPartsFont);
		decimationPartLb.setBackground(new Color(0.8f,0.8f,0.8f));
		decimationPartLb.setBounds(10, 260, 100, 30);
		advancedPanel.add(decimationPartLb);
		//QC
		DBox = new Checkbox("",true);
		DBox.setBounds(110,260,30,30);
		advancedPanel.add(DBox);

		//number of divisions
		Label deciReductionLb = new Label("Decimate reduction");
		deciReductionLb.setBackground(new Color(0.8f,0.8f,0.8f));
		deciReductionLb.setBounds(20, 290, 130, 30);
		advancedPanel.add(deciReductionLb);

		drVal = new TextField();
		drVal.setText("0.8");
		drVal.addKeyListener(kNumber);
		drVal.setBounds(200, 294, 40, 22);
		advancedPanel.add(drVal);

		
		
		Label autoDecimateThresholdLb = new Label("Auto decimate threshold");
		autoDecimateThresholdLb.setBackground(new Color(0.8f,0.8f,0.8f));
		autoDecimateThresholdLb.setBounds(10, 330, 180, 30);
		advancedPanel.add(autoDecimateThresholdLb);
		
		autoDecimateThresholdTF = new TextField();
		autoDecimateThresholdTF.setText("30");
		autoDecimateThresholdTF.addKeyListener(kNumberInt);
		autoDecimateThresholdTF.setBounds(200, 334, 40, 22);
		advancedPanel.add(autoDecimateThresholdTF);
		
		Label splitMeshActivateLb = new Label("Split large mesh");
		splitMeshActivateLb.setBackground(new Color(0.8f,0.8f,0.8f));
		splitMeshActivateLb.setBounds(10, 360, 180, 30);
		advancedPanel.add(splitMeshActivateLb);
		
		splitMeshActivate = new Checkbox("",true);
		splitMeshActivate.setBounds(200,360,30,30);
		advancedPanel.add(splitMeshActivate);
		
		Label splitMeshThreshLb = new Label("Split vertex threshold");
		splitMeshThreshLb.setBackground(new Color(0.8f,0.8f,0.8f));
		splitMeshThreshLb.setBounds(10, 390, 180, 30);
		advancedPanel.add(splitMeshThreshLb);
		
		splitMeshThreshold = new TextField();
		splitMeshThreshold.setText("100000");
		splitMeshThreshold.addKeyListener(kNumberInt);
		splitMeshThreshold.setBounds(200, 394, 60, 22);
		advancedPanel.add(splitMeshThreshold);

		
		//verbose
		Label verboseLb = new Label("Verbose level");
		verboseLb.setBackground(new Color(0.8f,0.8f,0.8f));
		verboseLb.setBounds(10, 420, 90, 30);
		advancedPanel.add(verboseLb);

		verboseValue = new Choice();
		verboseValue.add("0");
		verboseValue.add("1");
		verboseValue.add("2");
		verboseValue.select("2");
		verboseValue.setBounds(100, 424, 70, 30);
		advancedPanel.add(verboseValue);

		//3D viewer
		Label viewerLb = new Label("IJ 3D viewer");
		viewerLb.setBackground(new Color(0.8f,0.8f,0.8f));
		viewerLb.setBounds(10, 450, 150, 30);
		advancedPanel.add(viewerLb);
		
		ViewerBox = new Checkbox("",false);
		ViewerBox.setBounds(160,450,100,30);
		advancedPanel.add(ViewerBox);
		
		
		/**END OF ADVANCED MENU**/
		
		//background
		Label bckLb = new Label("Background");
		bckLb.setBackground(new Color(0.9f,0.9f,0.9f));
		bckLb.setBounds(450, 90, 75, 30);
		convertPanel.add(bckLb);

		bkgVal = new TextField();
		bkgVal.addKeyListener(kNumberInt);
		bkgVal.setText(String.valueOf(def_bkg));
		bkgVal.setBounds(530, 94, 50, 22);
		convertPanel.add(bkgVal);

		mm.add(advancedPanel);
		advancedPanel.setVisible(false);

		Button advancedConvert = new Button("Advanced");
		advancedConvert.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				if(uploadAdvancedPanel!=null) {
					uploadAdvancedPanel.setVisible(false);
				}
				if(!advancedPanel.isVisible()) {
					mm.setSize(1000,600);
					advancedPanel.setVisible(true);
				}else {
					mm.setSize(650,600);
					advancedPanel.setVisible(false);
				}
			}
		});
		advancedConvert.setBounds(450, 0, 130, 30);
		convertPanel.add(advancedConvert);


		//button to refresh opened images, upon refresh make sure to update voxel_size with currently selected!
		Button refreshImgBtn = new Button("Refresh images");
		refreshImgBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				imgChoice.removeAll();
				for(String i : WindowManager.getImageTitles()) {
					imgChoice.add(i);
				}
				ImagePlus curr;
				if((curr = WindowManager.getImage(imgChoice.getSelectedItem()))!=null) {
					voxel_sizes[0] = (float) curr.getCalibration().pixelWidth;
					voxel_sizes[1] = (float) curr.getCalibration().pixelHeight;
					voxel_sizes[2] = (float) curr.getCalibration().pixelDepth;
					vXvalue.setText(Float.toString(voxel_sizes[0]));
					vYvalue.setText(Float.toString(voxel_sizes[1]));
					vZvalue.setText(Float.toString(voxel_sizes[2]));
					
					//update background value
					int defbkg = GetLowestValueFromImage(curr);
					bkgVal.setText(String.valueOf(defbkg));
					
					if(uploadMenu) {
						uploadDatasetErrorLb.setVisible(false);
					}
					
					
				}
				
				if(datasetToUpdate!=null) {
					if(imgChoice.getSelectedItem()!=null) {
						datasetToUpdate.setText(removeExtensionFromFile(imgChoice.getSelectedItem()));
					}
				}
				if(uploadMenu) {
					if(userDatasetsNames.containsKey(datasetToUpdate.getText().toLowerCase())) {//if dataset already exists, put a warning label!
						uploadDatasetErrorLb.setVisible(true);
						uploadDatasetErrorLb.setText("Dataset already exists: check option below to replace time steps");
						datasetClear.setVisible(true);
						datasetClear.setState(true);
						create_dataset=false;
						dataset_update_id=userDatasetsNames.get(datasetToUpdate.getText().toLowerCase());
					}else {
						create_dataset=true;
						dataset_update_id=-1;
						uploadDatasetErrorLb.setVisible(false);
						datasetClear.setVisible(false);
						datasetClear.setState(false);
					}
				}
				
				
				//also refresh error messages
				if(WindowManager.getImageTitles().length==0) {
					missingImg=true;
					convertImgErrorLb.setVisible(true);
					convertBtn.setEnabled(false);
					defaultObjPath = "mesh.obj";
					fileChooser.setSelectedFile(new File(defaultDirPath+defaultObjPath));
					filePathTF.setText(defaultDirPath+defaultObjPath);
					
					
				}else {
					missingImg=false;
					convertImgErrorLb.setVisible(false);
					if(uploadMenu) {
						if(datasetToUpdate.getText()!="")
							convertBtn.setEnabled(true);
					}else {
						convertBtn.setEnabled(true);
					}
					//reset base mesh name
					defaultObjPath = imgChoice.getSelectedItem().split("\\.")[0]+".obj";
					fileChooser.setSelectedFile(new File(defaultDirPath+defaultObjPath));
					filePathTF.setText(defaultDirPath+defaultObjPath);
					
					if(ConfirmIfObjAlreadyExistsInPath(filePathTF.getText(),getFileFromPath(filePathTF.getText()))) {
						foundMeshesLabel.setVisible(true);
					}else {
						foundMeshesLabel.setVisible(false);
					}

					//also update time steps gui
					if(WindowManager.getImage(imgChoice.getSelectedItem()).getNFrames()>1) {
						minTimeStepLb.setText("Min step");
						maxTimeStepLb.setVisible(true);

						maxTSTF.setText(String.valueOf(WindowManager.getImage(imgChoice.getSelectedItem()).getNFrames()-1));
						maxTSTF.setVisible(true);
					}else {
						minTimeStepLb.setText("Time step");
						minTSTF.setText("0");
						maxTSTF.setVisible(false);
						maxTSTF.setText("-1");
						maxTimeStepLb.setVisible(false);
					}
				}
				
			}
		});
		refreshImgBtn.setBounds(450, 50, 130, 30);
		convertPanel.add(refreshImgBtn);

		//if stack, set time step, if hyperstack, select start & end

		minTimeStepLb = new Label("Time step");
		minTimeStepLb.setBackground(new Color(0.9f,0.9f,0.9f));
		minTimeStepLb.setBounds(50, 85, 75, 30);
		convertPanel.add(minTimeStepLb);

		minTSTF = new TextField("0");
		minTSTF.addKeyListener(kNumberInt);
		minTSTF.setBounds(130, 89, 40, 22);
		convertPanel.add(minTSTF);

		maxTimeStepLb = new Label("Max step");
		maxTimeStepLb.setBackground(new Color(0.9f,0.9f,0.9f));
		maxTimeStepLb.setBounds(200, 85, 90, 30);
		maxTimeStepLb.setVisible(false);
		convertPanel.add(maxTimeStepLb);

		maxTSTF = new TextField("-1");
		maxTSTF.addKeyListener(kNumberInt);
		maxTSTF.setBounds(300, 89, 40, 22);
		maxTSTF.setVisible(false);
		convertPanel.add(maxTSTF);
		
		Label channelLb = new Label("Channel");
		channelLb.setBackground(new Color(0.9f,0.9f,0.9f));
		channelLb.setBounds(50, 190, 75, 30);
		convertPanel.add(channelLb);
		
		channelTF = new TextField("0");
		channelTF.addKeyListener(kNumberInt);
		channelTF.setBounds(130, 194, 50, 22);
		convertPanel.add(channelTF);
		

		//if hyperstack:
		if(imgChoice.getSelectedItem()!=null) {
			if(WindowManager.getImage(imgChoice.getSelectedItem()).getNFrames()>1) {
				minTimeStepLb.setText("Min step");
				maxTimeStepLb.setVisible(true);

				maxTSTF.setText(String.valueOf(WindowManager.getImage(imgChoice.getSelectedItem()).getNFrames()-1));
				maxTSTF.setVisible(true);
			}
		}
		
	}
	
	
	/**
	 * UI building function to avoid copying code (all elements need to be static) - builds mesh path panel
	 * @param heightStart height at which the menu is initialized, as per java awt specifications (e.g : setBounds parameters)
	 * @param convertExists indicate if the convert menu exists in this instance of the plugin.
	 * @param uploadMenu determines if the upload menu will be present in this instance of the plugin.
	 */
	public static void BuildMeshPathMenu(int heightStart, boolean convertExists, boolean uploadMenu) {
		
		MeshPanel = new Panel();
		MeshPanel.setBounds(0, heightStart, 600, 90);
		MeshPanel.setLayout(null);
		MeshPanel.setBackground(new Color(0.9f,0.9f,0.9f));
		mm.add(MeshPanel);
		
		
		Label meshPathLb = new Label("Your meshes will be saved here");
		if(!convertExists) {
			meshPathLb.setText("Directory of meshes to upload");
		}
		meshPathLb.setBackground(new Color(0.9f,0.9f,0.9f));
		meshPathLb.setFont(mainPartFont);
		meshPathLb.setBounds(50, 0, 300, 30);
		MeshPanel.add(meshPathLb);

		//path chooser (file/path), if there is a loaded image, give it a custom name
		String defaultPath = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString();
		if(System.getProperty("os.name").toLowerCase().contains("mac")) {//change the path if we are on mac
			defaultPath = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop";
		}
		if(System.getProperty("os.name").toLowerCase().contains("linux")) {//change the path if we are on mac
			defaultPath = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop";
		}
		if(convertExists) {
			if(imgChoice.getSelectedItem()!=null) {
				try {
					defaultPath = defaultPath+File.separator+"MorphoNet"+File.separator+imgChoice.getSelectedItem().split("\\.")[0]+".obj";//
				}catch(Exception e) {
					IJ.error("input image name format incorrect");
				}
				
			}else{
				defaultPath = defaultPath+File.separator+"MorphoNet"+File.separator+"mesh.obj";
			}
		}else {
			defaultPath = defaultPath+File.separator+"MorphoNet"+File.separator+"mesh.obj";
		}
		
		//add the indicator of number of meshes present, only if convert menu does not exist (upload mode)
		numberOfFilesLb = new Label("X meshes will be uploaded");
		numberOfFilesLb.setBackground(new Color(0.9f,0.9f,0.9f));
		numberOfFilesLb.setBounds(50,64,475,20);
		if(convertExists) //only show if not in convert mode
			numberOfFilesLb.setVisible(false);
		numberOfFilesLb.setForeground(Color.RED);
		UpdateNumberOfFoundMeshesOnPath(removeFileFromPath(defaultPath),numberOfFilesLb);
		MeshPanel.add(numberOfFilesLb);
		
		filePathTF = new TextField(defaultPath);
		filePathTF.setEditable(false);
		filePathTF.setBounds(50, 40, 475, 22);
		MeshPanel.add(filePathTF);
		filePathTF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				if(!filePathTF.getText().equals("")) {//should test if it is a viable path ?
					convertPathErrorLb.setVisible(false);
					if(datasetToUpdate.getText()!="")
						convertBtn.setEnabled(true);
					if(ConfirmIfObjAlreadyExistsInPath(filePathTF.getText(),getFileFromPath(filePathTF.getText()))) {
						foundMeshesLabel.setVisible(true);
					}else {
						foundMeshesLabel.setVisible(false);
					}
				}else {
					convertPathErrorLb.setVisible(true);
					convertBtn.setEnabled(false);
				}
			}
		});

		
		foundMeshesLabel = new Label("WARNING : mesh(es) of this name already exists; may be overwritten"); 
		foundMeshesLabel.setBackground(new Color(0.9f,0.9f,0.9f));
		foundMeshesLabel.setBounds(50,60,475,30);
		foundMeshesLabel.setVisible(false);
		foundMeshesLabel.setForeground(Color.RED);
		MeshPanel.add(foundMeshesLabel);
		
		//if mesh already exists : put a warning message :
		if(ConfirmIfObjAlreadyExistsInPath(filePathTF.getText(),getFileFromPath(filePathTF.getText()))) {
			foundMeshesLabel.setVisible(true);
		}else {
			foundMeshesLabel.setVisible(false);
		}
		

		fileChooser = new JFileChooser();

		if(convertExists) {
			if(imgChoice.getSelectedItem()!=null)
				defaultObjPath = imgChoice.getSelectedItem().split("\\.")[0]+".obj";
			else
				defaultObjPath = "mesh.obj";
		}else {
			defaultObjPath = "mesh.obj";
		}
		
		
		fileChooser.setSelectedFile(new File(defaultPath));
		fileChooser.setFileFilter(new FileNameExtensionFilter("Wavefront meshes", "obj"));
		fileChooser.setAcceptAllFileFilterUsed(false);

		Button fileChooserBtn = new Button("Change location");
		fileChooserBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				if(fileChooser.showSaveDialog(null)==JFileChooser.APPROVE_OPTION) {
					filePathTF.setText(addObjExtensionToPath(fileChooser.getSelectedFile().getAbsolutePath()));
					if(convertPathErrorLb!=null)
						convertPathErrorLb.setVisible(false);
					
					//if in upload mode, update label for nb of meshes found
					UpdateNumberOfFoundMeshesOnPath(removeFileFromPath(filePathTF.getText()),numberOfFilesLb);
					
					//also update final upload path automatically
					if(uploadMenu)
						pathToUploadTF.setText(removeFileFromPath(addObjExtensionToPath(fileChooser.getSelectedFile().getAbsolutePath())));
					
					//update number of found meshes
					if(ConfirmIfObjAlreadyExistsInPath(filePathTF.getText(),getFileFromPath(filePathTF.getText()))) {
						foundMeshesLabel.setVisible(true);
					}else {
						foundMeshesLabel.setVisible(false);
					}
					
					if(convertExists) {
						if(imgChoice.getSelectedItem()!=null) {//upon obj file selection, if an image is selected, make the convertbutton usable
							if(datasetToUpdate.getText()!="")
								convertBtn.setEnabled(true);
						}
					}
					
					
					//update upload file selector too !
					if(fileToUploadChoice!=null) {
						try {
							for(String s : getObjMeshesInPath(filePathTF.getText())) {
								fileToUploadChoice.add(s);
							}
						} catch (IOException e1) {
							System.out.println("ERROR getObjMeshesInPath : "+e1);
						}
					}
				}
			}
		});
		fileChooserBtn.setBounds(450, 0, 130, 30);
		MeshPanel.add(fileChooserBtn);
	}
		
	/**
	 * UI building function to avoid copying code (all elements need to be static) - builds connect & upload menu
	 * @param heightStart height at which the menu is initialized, as per java awt specifications (e.g : setBounds parameters)
	 * @param uploadBtn determines if the upload button is present in this instance of the plugin
	 */
	public static void BuildUploadMenu(int heightStart, boolean uploadBtn) {
		
		//when this menu is initialized, create a list of the current user's datasets (that will be updated in real-time, to detect doubles)
		try {
			userDatasetsNames = GetDatasetsDict();
		} catch (IOException e1) {
			System.out.println("BuildUploadMenu : could not initialize user dataset names");;
		}
		
		//put all except connect in a panel so it is easier to show/hide
			uploadPanel = new Panel();
			uploadPanel.setBounds(50, heightStart, 550, 300);
			uploadPanel.setLayout(null);
			uploadPanel.setBackground(new Color(0.9f,0.9f,0.9f));
			uploadPanel.setVisible(false);
			mm.add(uploadPanel);

			Label uploadToMNLb = new Label("Upload meshes to MorphoNet");
			uploadToMNLb.setBackground(new Color(0.9f,0.9f,0.9f));
			uploadToMNLb.setFont(mainPartFont);
			uploadToMNLb.setBounds(50, heightStart, 350, 30);
			mm.add(uploadToMNLb);
			
			Label uploadToMNLb2 = new Label("Upload meshes to MorphoNet");
			uploadToMNLb2.setBackground(new Color(0.9f,0.9f,0.9f));
			uploadToMNLb2.setFont(mainPartFont);
			uploadToMNLb2.setBounds(0, 0, 350, 30);
			uploadPanel.add(uploadToMNLb2);
			
			

			uploadDatasetErrorLb = new Label("Needs a dataset!");
			uploadDatasetErrorLb.setForeground(Color.RED);
			uploadDatasetErrorLb.setBackground(new Color(0.9f,0.9f,0.9f));
			uploadDatasetErrorLb.setBounds(0, 65, 380, 20);
			uploadPanel.add(uploadDatasetErrorLb);
			
			

			datasetClear = new Checkbox("Dataset replace",false);
			datasetClear.setBackground(new Color(0.9f,0.9f,0.9f));
			datasetClear.setBounds(100, 90, 150, 20);
			uploadPanel.add(datasetClear);
			datasetClear.setVisible(false);
			
			//2 modes, just the connect button if not connected, and the rest of the UI if we are

			Label datasetLb = new Label("Dataset name");
			datasetLb.setBackground(new Color(0.9f,0.9f,0.9f));
			datasetLb.setBounds(0, 40, 100, 30);
			uploadPanel.add(datasetLb);
			
			//text field for the dataset choice (unexisting will create new, and existing (using picked option) will upload to already created
			String defDatasetName="";
			if(!uploadBtn) {
				if(imgChoice!=null) {
					if(imgChoice.getSelectedItem()!=null) {
						defDatasetName = removeExtensionFromFile(imgChoice.getSelectedItem());
					}
				}
			}
			
			if(userDatasetsNames.containsKey(defDatasetName.toLowerCase())) {//if dataset already exists, put a warning label!
				uploadDatasetErrorLb.setVisible(true);
				uploadDatasetErrorLb.setText("Dataset already exists: check option below to replace time steps");
				datasetClear.setVisible(true);
				datasetClear.setState(true);
				create_dataset=false;
				dataset_update_id=userDatasetsNames.get(defDatasetName.toLowerCase());
			}
			
			datasetToUpdate = new TextField(defDatasetName);
			datasetToUpdate.setBounds(100, 43, 280, 22);
			uploadPanel.add(datasetToUpdate);
			datasetToUpdate.addKeyListener(new KeyListener() {

				@Override
				public void keyPressed(KeyEvent arg0) {

				}

				@Override
				public void keyReleased(KeyEvent arg0) {
					if(!datasetToUpdate.getText().equals("")) {
						uploadDatasetErrorLb.setVisible(false);

						if(imgChoice!=null) {
							if(imgChoice.getSelectedItem()!=null)
								convertBtn.setEnabled(true);
						}
						
						uploadToMNBtn.setEnabled(true);
						
						if(uploadBtn && numberOfFilesLb.getText().substring(0, 1).equals("0"))//if in upload mode, make sure there are meshes to upload too
						{
							uploadToMNBtn.setEnabled(false);
						}
						
						
						if(userDatasetsNames.containsKey(datasetToUpdate.getText().toLowerCase())) {//if dataset already exists, put a warning label!
							uploadDatasetErrorLb.setVisible(true);
							uploadDatasetErrorLb.setText("Dataset already exists: check option below to replace time steps");
							datasetClear.setVisible(true);
							datasetClear.setState(true);
							create_dataset=false;
							dataset_update_id=userDatasetsNames.get(datasetToUpdate.getText().toLowerCase());
						}else {
							create_dataset=true;
							dataset_update_id=-1;
							uploadDatasetErrorLb.setVisible(false);
							datasetClear.setVisible(false);
							datasetClear.setState(false);
						}
							

					}else {
						create_dataset=true;
						dataset_update_id=-1;
						uploadDatasetErrorLb.setVisible(true);
						uploadDatasetErrorLb.setText("Needs a dataset !");
						uploadToMNBtn.setEnabled(false);
						if(convertBtn!=null)
							convertBtn.setEnabled(false);
					}
				}

				@Override
				public void keyTyped(KeyEvent arg0) {
				}
				
			});
			
			if(!datasetToUpdate.getText().equals("") && !userDatasetsNames.containsKey(defDatasetName.toLowerCase())) {
				uploadDatasetErrorLb.setVisible(false);
			}
			
			
			//button to create a new dataset after choosing option pick
			createNewDataset = new Button("Create new Dataset");
			createNewDataset.setBounds(400, 80, 130, 30);
			createNewDataset.setVisible(false);
			createNewDataset.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent a) {//open up side GUI for picking
					datasetToUpdate.setEditable(true);
					datasetToUpdate.setText("");
					createNewDataset.setVisible(false);
					uploadToMNBtn.setEnabled(false);
					uploadDatasetErrorLb.setVisible(false);
					datasetClear.setVisible(false);
					datasetClear.setState(false);
					create_dataset=true;
				}
			});
			uploadPanel.add(createNewDataset);

			//pick existing dataset button
			Button pickDatasetBtn = new Button("Pick existing dataset");
			pickDatasetBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent a) {//open up side GUI for picking
					try {
						GetMyDatasets();
						
						
						
					} catch (NoSuchAlgorithmException e) {
						System.out.println("NoSuchAlgorithmException on GetMyDatasets: "+e);
					} catch (IOException e) {
						System.out.println("IOEXCEPTION on GetMyDatasets: "+e);
					}
				}
			});
			pickDatasetBtn.setBounds(400, 40, 130, 30);
			uploadPanel.add(pickDatasetBtn);

			
			
			//macos style exception
			if(System.getProperty("os.name").toLowerCase().contains("mac")) {//change the path if we are on mac
				pickDatasetBtn.setLabel("<html>Pick existing dataset</html>");
				pickDatasetBtn.setBounds(400, 20, 130, 50);
				createNewDataset.setBounds(400, 100, 130, 30);
			}
			//Upload menu advanced button : 
			uploadAdvancedPanel = new Panel();
			uploadAdvancedPanel.setBackground(new Color(0.8f,0.8f,0.8f));
			uploadAdvancedPanel.setBounds(600, 50, 250, 300);
			uploadAdvancedPanel.setLayout(null);
			uploadAdvancedPanel.setVisible(false);
			mm.add(uploadAdvancedPanel);
			
			Label centerLb = new Label("Center");
			centerLb.setBackground(new Color(0.8f,0.8f,0.8f));
			centerLb.setBounds(10, 10, 75, 30);
			uploadAdvancedPanel.add(centerLb);
			
			centerB = new Checkbox();
			centerB.setState(true);
			centerB.setBounds(120, 10, 40, 30);
			uploadAdvancedPanel.add(centerB);
			
			
			
			Label qualityLb = new Label("Quality");
			qualityLb.setBackground(new Color(0.8f,0.8f,0.8f));
			qualityLb.setBounds(10, 40, 75, 30);
			uploadAdvancedPanel.add(qualityLb);
			

			
			qualityTF = new JSlider(JSlider.HORIZONTAL,0,5,0);
			qualityTF.setBounds(85, 40, 140, 50);
			qualityTF.setBackground(new Color(0.8f,0.8f,0.8f));
			qualityTF.setMajorTickSpacing(1);
			qualityTF.setPaintLabels(true);
			qualityTF.setPaintTicks(true);
			uploadAdvancedPanel.add(qualityTF);
			
			
			
			Button uploadAdvancedBtn = new Button("Advanced");
			uploadAdvancedBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent a) {//open up side GUI for picking
					if(advancedPanel!=null) {
						advancedPanel.setVisible(false);
					}
					if(!uploadAdvancedPanel.isVisible()) {
						if(uploadBtn) {
							mm.setSize(900,500);
						}else {
							mm.setSize(900,600);
						}
						uploadAdvancedPanel.setVisible(true);
					}else {
						if(uploadBtn) {
							mm.setSize(650,500);
						}else {
							mm.setSize(650,600);
						}
						uploadAdvancedPanel.setVisible(false);
					}
				}
			});
			uploadAdvancedBtn.setBounds(400, 80, 130, 30);
			uploadPanel.add(uploadAdvancedBtn);
			

			JFileChooser uploadPathChooser = new JFileChooser();

			uploadPathChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			uploadPathChooser.setSelectedFile(new File(defaultDirPath));
			uploadPathChooser.setFileFilter(new FileNameExtensionFilter("Wavefront meshes", "obj"));
			
			
			Label fileUpLb = new Label("Upload path");
			fileUpLb.setBackground(new Color(0.9f,0.9f,0.9f));
			fileUpLb.setBounds(0, 120, 100, 30);
			fileUpLb.setVisible(false);
			uploadPanel.add(fileUpLb);

			//use filesUploadReminder to remind user how many files we are going to upload
			pathToUploadTF = new TextField(defaultDirPath);
			pathToUploadTF.setBounds(100, 124, 220, 22);
			pathToUploadTF.setVisible(false);
			uploadPanel.add(pathToUploadTF);
			
			

			//final reminder : 
			filesUploadReminder = new Label("X files will be uploaded to MorphoNet");
			filesUploadReminder.setBackground(new Color(0.9f,0.9f,0.9f));
			filesUploadReminder.setForeground(new Color(1.0f,0.0f,0.0f));
			filesUploadReminder.setBounds(0, 130, 250, 30);
			filesUploadReminder.setVisible(false);
			uploadPanel.add(filesUploadReminder);
			



			//upload to morphonet button
			uploadToMNBtn = new Button("Upload");
			uploadToMNBtn.setEnabled(false);
			uploadToMNBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent a) {//open up side GUI for picking
					uploadToMNBtn.setEnabled(false);
					//manage progressbar
					UploadProgress.setVisible(true);
					UploadProgress.setMinimum(0);
					UploadProgress.setMaximum(1);
					UploadProgress.setValue(0);
					int uploadProgressValue=0;
					
					if(create_dataset) {//only create dataset if you did not pick one !
						if(!datasetToUpdate.getText().equals("")) {
							try {
								System.out.println("CREATING DATASET : "+datasetToUpdate.getText());
								CreateDataset(datasetToUpdate.getText());
							} catch (NoSuchAlgorithmException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}

						}
					}
					if(dataset_update_id!=-1) {//only update if we have a valid dataset_id, either created or picked !
						
						try {
							
							//check if uploading a file or an entire directory
							File uploadpath = new File(pathToUploadTF.getText());
							if(uploadpath.exists()) {
								if(uploadpath.isDirectory()) {//dir case
									List<String> objs = getObjMeshesInPath(pathToUploadTF.getText());
									
									UploadProgress.setMaximum(objs.size());
									
									for(String s : objs) {
										if(datasetClear.getState())
											ClearDatasetAtTimePoint(GetTimeStepFromMeshName(s),String.valueOf(qualityTF.getValue()),channelTF.getText());
										AddObjFileToDataset(s,GetTimeStepFromMeshName(s),String.valueOf(qualityTF.getValue()),centerB.getState());
										uploadProgressValue++;
										UploadProgress.setValue(uploadProgressValue);
									}
									
									
								}else {//file case
									if(datasetClear.getState())
										ClearDatasetAtTimePoint(GetTimeStepFromMeshName(pathToUploadTF.getText()),String.valueOf(qualityTF.getValue()),channelTF.getText());
									AddObjFileToDataset(pathToUploadTF.getText(),GetTimeStepFromMeshName(pathToUploadTF.getText()),String.valueOf(qualityTF.getValue()),centerB.getState());
								}
								if(seeDatasetInMN!=null) {//make the see on MN button appear
						        	seeDatasetInMN.setVisible(true);
						        }
							}
						}catch(Exception e) {
							IJ.error("error during file upload !"+e );
						}
						
					}

					uploadToMNBtn.setEnabled(true);
				}
			});
			uploadToMNBtn.setBackground(new Color(0.26f,0.83f,1.0f));
			uploadToMNBtn.setBounds(400, 160, 130, 40);
			if(uploadBtn)
				uploadPanel.add(uploadToMNBtn);

			uploadObjErrorLb = new Label("", Label.CENTER);
			uploadObjErrorLb.setForeground(Color.RED);
			uploadObjErrorLb.setBackground(new Color(0.9f,0.9f,0.9f));
			uploadObjErrorLb.setBounds(400, 210, 130, 20);
			uploadPanel.add(uploadObjErrorLb);


			
			//update error messages and mesh upload number
			UpdateNumberOfUploadMeshesInPath(defaultDirPath);


			
			seeDatasetInMN = new Button("See on MorphoNet");
			seeDatasetInMN.setBackground(new Color(0.0f,0.78f,0.33f));
			seeDatasetInMN.setBounds(145, 160, 170, 40);
			seeDatasetInMN.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent a) {
					SeeDatasetOnMN();
				}
			});
			uploadPanel.add(seeDatasetInMN);
			seeDatasetInMN.setVisible(false);
			
			
			uploadPanel.setVisible(true);
	}


	/**
	 * build the GUI for the upload Images submenu. Needs to be together with the Upload menu (that does not have the upload button activated)
	 * @param heightStart height at which the menu is initialized, as per java awt specifications (e.g : setBounds parameters)
	 * @param datasetSelector determines if the dataset selector will be present in this instance of the plugin
	 * @param refreshBtn determines if the refresh images button will be present in this instance of the plugin.
	 * @param uploadImgBtn determines if the upload images button will be present in this instance of the plugin
	 */
	public static void BuildImagesMenu(int heightStart, boolean datasetSelector, boolean refreshBtn, boolean uploadImgBtn) {
		
		//when this menu is initialized, create a list of the current user's datasets (that will be updated in real-time, to detect doubles)
		
		
		//put all except connect in a panel so it is easier to show/hide
		imagesPanel = new Panel();
		imagesPanel.setBounds(50, heightStart, 550, 400);
		imagesPanel.setLayout(null);
		imagesPanel.setBackground(new Color(0.9f,0.9f,0.9f));
		mm.add(imagesPanel);
		
		
		Label convertLb = new Label("Upload Images to Datasets");
		convertLb.setBackground(new Color(0.9f,0.9f,0.9f));
		convertLb.setFont(mainPartFont);
		convertLb.setBounds(0, 0, 350, 30);
		imagesPanel.add(convertLb);

		int selectorOffset=0;
		
		if(datasetSelector) {
			Label datasetLb = new Label("Dataset name");
			datasetLb.setBackground(new Color(0.9f,0.9f,0.9f));
			datasetLb.setBounds(0, 40, 100, 30);
			imagesPanel.add(datasetLb);
			
			datasetMissingLb = new Label("Please select a dataset!");
			datasetMissingLb.setForeground(Color.RED);
			datasetMissingLb.setBackground(new Color(0.9f,0.9f,0.9f));
			datasetMissingLb.setBounds(0, 67, 200, 18);
			imagesPanel.add(datasetMissingLb);
			
			
			PickDatasetField = new Label("Empty");
			PickDatasetField.setBounds(100, 40, 280, 30);
			imagesPanel.add(PickDatasetField);
			
			

			//pick existing dataset button
			Button pickDatasetBtn = new Button("Pick dataset");
			pickDatasetBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent a) {//open up side GUI for picking
					try {
						GetMyDatasets();
					} catch (NoSuchAlgorithmException e) {
						System.out.println("NoSuchAlgorithmException on GetMyDatasets: "+e);
					} catch (IOException e) {
						System.out.println("IOEXCEPTION on GetMyDatasets: "+e);
					}
				}
			});
			pickDatasetBtn.setBounds(400, 40, 130, 30);
			imagesPanel.add(pickDatasetBtn);

			

		}else {
			selectorOffset=60;
		}
		

		//convert error message label
		ImagesImgErrorLb = new Label("Needs an image!");
		ImagesImgErrorLb.setForeground(Color.RED);
		ImagesImgErrorLb.setBackground(new Color(0.9f,0.9f,0.9f));
		ImagesImgErrorLb.setBounds(0, 122-selectorOffset, 130, 18);
		imagesPanel.add(ImagesImgErrorLb);

		//image choice
		Label fileChoiceLb = new Label("Input image");
		fileChoiceLb.setBackground(new Color(0.9f,0.9f,0.9f));
		fileChoiceLb.setBounds(0,100-selectorOffset,75,20);
		imagesPanel.add(fileChoiceLb);

		ImageImgChoice = new Choice();
		for(String i : WindowManager.getImageTitles()) {
			ImageImgChoice.add(i);
		}

		ImageImgChoice.setBounds(100, 100-selectorOffset, 250, 20);
		imagesPanel.add(ImageImgChoice);

		//update error message if no images !
		if(WindowManager.getImageTitles().length==0) {
		}else {
			ImagesImgErrorLb.setVisible(false);
		}

		ImageImgChoice.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) { //on changing item, update voxel_size
				
				if(WindowManager.getImage(ImageImgChoice.getSelectedItem()).getNFrames()>1){
					imgMaxTSTF.setVisible(true);
					imgMaxTSTF.setText(String.valueOf(WindowManager.getImage(ImageImgChoice.getSelectedItem()).getNFrames()-1));
					imgMaxTimeStepLb.setVisible(true);
				}else {
					imgMaxTSTF.setVisible(false);
					imgMaxTSTF.setText("-1");
					imgMaxTimeStepLb.setVisible(false);
				}
				
				ImagePlus curr;
				float[] voxel_sizes = {1.0f,1.0f,1.0f};
				if((curr = WindowManager.getImage(e.getItem().toString()))!=null) {
					voxel_sizes[0] = (float) curr.getCalibration().pixelWidth;
					voxel_sizes[1] = (float) curr.getCalibration().pixelHeight;
					voxel_sizes[2] = (float) curr.getCalibration().pixelDepth;
					vXvalue.setText(Float.toString(voxel_sizes[0]));
					vYvalue.setText(Float.toString(voxel_sizes[1]));
					vZvalue.setText(Float.toString(voxel_sizes[2]));
				}

			}

		});
		
		//time step selector
		
		if(refreshBtn) {
			Button refreshImgBtn = new Button("Refresh images");
			refreshImgBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent a) {
					if(WindowManager.getImageTitles().length==0) {
						ImagesImgErrorLb.setVisible(true);
						UploadImageToMN.setEnabled(false);
						
						//hide 4d image UI if there are not images
						imgMaxTSTF.setVisible(false);
						imgMaxTSTF.setText("-1");
						imgMaxTimeStepLb.setVisible(false);
					}else {//if there are images, remove the error
						ImagesImgErrorLb.setVisible(false);
						if(dataset_update_id!=-1 && !PickDatasetField.getText().equals("") ) {//and also if we have a selected dataset
							UploadImageToMN.setEnabled(true);
						}
						
						//if there is an image, and it is 4D, change the ui appropriately.
						if(WindowManager.getImage(WindowManager.getImageTitles()[0]).getNFrames()>1){
							imgMaxTSTF.setVisible(true);
							imgMaxTSTF.setText(String.valueOf(WindowManager.getImage(WindowManager.getImageTitles()[0]).getNFrames()-1));
							imgMaxTimeStepLb.setVisible(true);
						}else {
							imgMaxTSTF.setVisible(false);
							imgMaxTSTF.setText("-1");
							imgMaxTimeStepLb.setVisible(false);
						}
						//if there are images : 
						ImagePlus curr;
						float[] voxel_sizes = {1.0f,1.0f,1.0f};
						if((curr = WindowManager.getImage(WindowManager.getImageTitles()[0]))!=null) {
							voxel_sizes[0] = (float) curr.getCalibration().pixelWidth;
							voxel_sizes[1] = (float) curr.getCalibration().pixelHeight;
							voxel_sizes[2] = (float) curr.getCalibration().pixelDepth;
							vXvalue.setText(Float.toString(voxel_sizes[0]));
							vYvalue.setText(Float.toString(voxel_sizes[1]));
							vZvalue.setText(Float.toString(voxel_sizes[2]));
						}
					}
					ImageImgChoice.removeAll();
					for(String i : WindowManager.getImageTitles()) {
						ImageImgChoice.add(i);
					}
					
					
				}
			});
			refreshImgBtn.setBounds(400, 96-selectorOffset, 130, 30);
			imagesPanel.add(refreshImgBtn);
		}
		
		
		imgMinTimeStepLb = new Label("Time step");
		imgMinTimeStepLb.setBackground(new Color(0.9f,0.9f,0.9f));
		imgMinTimeStepLb.setBounds(0, 150-selectorOffset, 75, 30);
		imagesPanel.add(imgMinTimeStepLb);

		imgMinTSTF = new TextField("0");
		imgMinTSTF.addKeyListener(kNumberInt);
		imgMinTSTF.setBounds(100, 154-selectorOffset, 40, 22);
		imagesPanel.add(imgMinTSTF);

		imgMaxTimeStepLb = new Label("Max step");
		imgMaxTimeStepLb.setBackground(new Color(0.9f,0.9f,0.9f));
		imgMaxTimeStepLb.setBounds(150, 150-selectorOffset, 90, 30);
		imgMaxTimeStepLb.setVisible(false);
		imagesPanel.add(imgMaxTimeStepLb);

		imgMaxTSTF = new TextField("-1");
		imgMaxTSTF.addKeyListener(kNumberInt);
		imgMaxTSTF.setBounds(250, 154-selectorOffset, 40, 22);
		imgMaxTSTF.setVisible(false);
		imagesPanel.add(imgMaxTSTF);
		
		if(WindowManager.getImageCount()>0) {
			//if there is an image, and it is 4D, change the ui appropriately.
			if(WindowManager.getImage(ImageImgChoice.getSelectedItem()).getNFrames()>1){
				imgMaxTSTF.setVisible(true);
				imgMaxTSTF.setText(String.valueOf(WindowManager.getImage(ImageImgChoice.getSelectedItem()).getNFrames()-1));
				imgMaxTimeStepLb.setVisible(true);
			}else {
				imgMaxTSTF.setVisible(false);
				imgMaxTSTF.setText("-1");
				imgMaxTimeStepLb.setVisible(false);
			}
		}
		
		//factor scale :
		Label factorScaleLb = new Label("Downscale");
		factorScaleLb.setBackground(new Color(0.9f,0.9f,0.9f));
		factorScaleLb.setBounds(0,210-selectorOffset,75,20);
		imagesPanel.add(factorScaleLb);
		
		Label factorScaleXYLb = new Label("XY");
		factorScaleXYLb.setBackground(new Color(0.9f,0.9f,0.9f));
		factorScaleXYLb.setBounds(100,210-selectorOffset,30,20);
		imagesPanel.add(factorScaleXYLb);

		rawImgScaleSlider = new JSlider(JSlider.HORIZONTAL,1,4,4);
		rawImgScaleSlider.setBounds(140, 190-selectorOffset, 120, 60);
		rawImgScaleSlider.setMajorTickSpacing(1);
		rawImgScaleSlider.setBackground(new Color(0.9f,0.9f,0.9f));
		rawImgScaleSlider.setPaintLabels(true);
		rawImgScaleSlider.setPaintTicks(true);
		imagesPanel.add(rawImgScaleSlider);
		
		Label factorScaleZLb = new Label("Z");
		factorScaleZLb.setBackground(new Color(0.9f,0.9f,0.9f));
		factorScaleZLb.setBounds(300,210-selectorOffset,30,20);
		imagesPanel.add(factorScaleZLb);
		
		ZfactorScaleSlider = new JSlider(JSlider.HORIZONTAL,1,4,4);
		ZfactorScaleSlider.setBounds(340, 190-selectorOffset, 120, 60);
		ZfactorScaleSlider.setMajorTickSpacing(1);
		ZfactorScaleSlider.setBackground(new Color(0.9f,0.9f,0.9f));
		ZfactorScaleSlider.setPaintLabels(true);
		ZfactorScaleSlider.setPaintTicks(true);
		imagesPanel.add(ZfactorScaleSlider);
		
		rawImgScaleSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				ZfactorScaleSlider.setValue(rawImgScaleSlider.getValue());
		      }
		});
		
		
		Label rawImgChannel = new Label("Channel");
		rawImgChannel.setBackground(new Color(0.9f,0.9f,0.9f));
		rawImgChannel.setBounds(0, 270-selectorOffset, 75, 30);
		imagesPanel.add(rawImgChannel);

		TextField rawImgChannelTF = new TextField("0");
		rawImgChannelTF.addKeyListener(kNumberInt);
		rawImgChannelTF.setBounds(100, 274-selectorOffset, 40, 22);
		imagesPanel.add(rawImgChannelTF);
		
		Label voxelSizeLb = new Label("Voxel size");
		voxelSizeLb.setBackground(new Color(0.9f,0.9f,0.9f));
		voxelSizeLb.setBounds(0, 320-selectorOffset, 75, 30);
		imagesPanel.add(voxelSizeLb);
		
		//init voxel_size : 
		ImagePlus curr;
		float[] voxel_sizes = {1.f,1.f,1.f};
		if((curr = WindowManager.getImage(ImageImgChoice.getSelectedItem()))!=null) {
			voxel_sizes[0] = (float) curr.getCalibration().pixelWidth;
			voxel_sizes[1] = (float) curr.getCalibration().pixelHeight;
			voxel_sizes[2] = (float) curr.getCalibration().pixelDepth;
		}
		
		

		vXvalue = new TextField();
		vXvalue.setText(Float.toString(voxel_sizes[0]));
		vXvalue.setBounds(100, 324-selectorOffset, 90, 22);
		vXvalue.addKeyListener(kNumber);
		imagesPanel.add(vXvalue);
		vYvalue = new TextField();
		vYvalue.setText(Float.toString(voxel_sizes[1]));
		vYvalue.setBounds(195, 324-selectorOffset, 90, 22);
		vYvalue.addKeyListener(kNumber);
		imagesPanel.add(vYvalue);
		vZvalue = new TextField();
		vZvalue.setText(Float.toString(voxel_sizes[2]));
		vZvalue.setBounds(290, 324-selectorOffset, 90, 22);
		vZvalue.addKeyListener(kNumber);
		imagesPanel.add(vZvalue);
		
		if(uploadImgBtn) {
			UploadImageToMN = new Button("Upload image");
			UploadImageToMN.setBackground(new Color(0.26f,0.83f,1.0f));
			UploadImageToMN.setBounds(400, 360-selectorOffset, 130, 30);
			UploadImageToMN.setEnabled(false);
			imagesPanel.add(UploadImageToMN);
			UploadImageToMN.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent a) {
					
					if(WindowManager.getImage(ImageImgChoice.getSelectedItem())!=null) {
						File f = new File(LocalMNPath+"tempImage");
						if(f.exists() && !f.isDirectory()) {
							f.delete();
						}
						
						if(WindowManager.getImage(ImageImgChoice.getSelectedItem()).getType()==ImagePlus.GRAY8) {//make sure image is 8Bit Gray
							int progressVal=0;
							ImageProgress.setVisible(true);
							ImageProgress.setValue(progressVal);
							ImageProgress.setMinimum(0);
							ImageProgress.setMaximum(Integer.parseInt(imgMaxTSTF.getText())-Integer.parseInt(imgMinTSTF.getText()));
							if(Integer.parseInt(imgMaxTSTF.getText())==-1)
								ImageProgress.setMaximum(1);
							 
							int i=Integer.parseInt(imgMinTSTF.getText());
							do {
								
								//make a downscaled version of the image and save it next to the base one
								
								ImagePlus imp = WindowManager.getImage(ImageImgChoice.getSelectedItem());
								
								ImagePlus impB = imp.duplicate();
								if(imp.getNFrames()>1){
									impB = getFrameFrom4DImagePlus(imp,imp.getNFrames(),i+1);
								}
								
								ImagePlus impR = DownScaleImagePlusNearestNeighbor(impB,rawImgScaleSlider.getValue(),ZfactorScaleSlider.getValue());
								try {
									SaveImagePlusAsByteArray(impR,LocalMNPath+"tempImage");
								} catch (IOException e1) {
									e1.printStackTrace();
								}
								
								
								
									if(impR.getHeight()*impR.getWidth()*impR.getNSlices()<=10000000) {//make sure the image has less than 10M voxels
										System.out.println("Uploading RAW Image "+ImageImgChoice.getSelectedItem()+" for t "+i);
										try {
											float scale = rawImgScaleSlider.getValue();
											float z = Float.parseFloat(vZvalue.getText());
											if(rawImgScaleSlider.getValue()!=ZfactorScaleSlider.getValue()) {//if factor is different, scale voxel_size z
												z /= (rawImgScaleSlider.getValue()/ZfactorScaleSlider.getValue());
											}
											String voxel_size = vXvalue.getText()+","+vYvalue.getText()+","+z;
											ClearRawImageFromDataset(String.valueOf(i),rawImgChannelTF.getText());
											AddRawImageToDataset(LocalMNPath+"tempImage",String.valueOf(i),scale, impR, rawImgChannelTF.getText(),voxel_size);
											progressVal++;
											ImageProgress.setValue(progressVal);
										}catch(Exception e) {
											System.out.println("error while uploading image : "+e);
										}
									}else {
										IJ.error("Error : The image you are trying to upload at t"+i+" is too big : it must contain less than 10 Million voxels. Please use a higher Downscaling factor.");
									}
									
								
								
								if(f.exists() && !f.isDirectory()) {
									f.delete();
								}
								
								i++;
								impR.changes=false;
								impR.close();
								impB.changes=false;
								impB.close();
							}while(i<=Integer.parseInt(imgMaxTSTF.getText()));
							seeDatasetInMN.setVisible(true);
						}else {
							IJ.error("Error : the image you are trying to upload is not in 8-Bit Greyscale. Please convert it before uploading (Image>Type>8Bit tab)");
						}
						
						if(f.exists() && !f.isDirectory()) {
							f.delete();
						}
					}else {
						IJ.error("Error : the specified images is not opened on Fiji ! please click on the refresh images button or load it again.");
					}
					
					
					
					
				}
			});
		}
		
		
		seeDatasetInMN = new Button("See on MorphoNet");
		seeDatasetInMN.setBackground(new Color(0.0f,0.78f,0.33f));
		seeDatasetInMN.setBounds(145, 360-selectorOffset, 170, 30);
		seeDatasetInMN.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				SeeDatasetOnMN();
			}
		});
		imagesPanel.add(seeDatasetInMN);
		seeDatasetInMN.setVisible(false);
		
		
	}
	
	/**
	 * UI building function to Build features menu
	 * @param heightStart height at which the menu is initialized, as per java awt specifications (e.g : setBounds parameters)
	 * @param datasetSelector determines if the dataset selector is present in this instance of the plugin
	 * @param uploadButton determines if the upload button is present in this instance of the plugin
	 */
	public static void BuildFeaturesMenu(int heightStart, boolean datasetSelector ,boolean uploadButton) {
		/**Convert part**/
		
		infosPanel = new Panel();
		infosPanel.setBackground(new Color(0.9f,0.9f,0.9f));
		infosPanel.setBounds(50, heightStart, 600, 600);
		infosPanel.setLayout(null);
		mm.add(infosPanel);
		
		JLabel convertLb = new JLabel("Upload Features to Datasets");
		convertLb.setBackground(new Color(0.9f,0.9f,0.9f));
		convertLb.setFont(mainPartFont);
		convertLb.setBounds(0, 0, 500, 30);
		infosPanel.add(convertLb);
		
		
		
		
		Label datasetLb = new Label("Dataset name");
		datasetLb.setBackground(new Color(0.9f,0.9f,0.9f));
		datasetLb.setBounds(0, 40, 100, 30);
		infosPanel.add(datasetLb);
		
		datasetMissingLb = new Label("Please select a dataset!");
		datasetMissingLb.setForeground(Color.RED);
		datasetMissingLb.setBackground(new Color(0.9f,0.9f,0.9f));
		datasetMissingLb.setBounds(0, 67, 200, 18);
		infosPanel.add(datasetMissingLb);
		
		
		PickDatasetField = new Label("Empty");
		PickDatasetField.setBounds(100, 40, 280, 30);
		infosPanel.add(PickDatasetField);
		
		

		//pick existing dataset button
		Button pickDatasetBtn = new Button("Pick dataset");
		pickDatasetBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {//open up side GUI for picking
				try {
					GetMyDatasets();
				} catch (NoSuchAlgorithmException e) {
					System.out.println("NoSuchAlgorithmException on GetMyDatasets: "+e);
				} catch (IOException e) {
					System.out.println("IOEXCEPTION on GetMyDatasets: "+e);
				}
			}
		});
		pickDatasetBtn.setBounds(400, 40, 130, 30);
		infosPanel.add(pickDatasetBtn);

		//line between 2 parts
		JPanel dline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		dline.setBounds(0, 90, 550, 1);
		infosPanel.add(dline);
		
		

		/**----Menu for info select from Fiji arrays----**/
		
		
		Label resultsChoiceLb = new Label("Results");
		resultsChoiceLb.setBackground(new Color(0.9f,0.9f,0.9f));
		resultsChoiceLb.setBounds(0, 110, 75, 20);
		infosPanel.add(resultsChoiceLb);
		
		Choice resultsChoice = new Choice();
		resultsChoice.setBounds(100,110,275,20);
		infosPanel.add(resultsChoice);
		
		for(String s : WindowManager.getNonImageTitles()) {
			resultsChoice.add(s);
		}
		
		
		Label featureObjIdLb = new Label("Object ID");
		featureObjIdLb.setBackground(new Color(0.9f,0.9f,0.9f));
		featureObjIdLb.setBounds(0, 140, 75, 20);
		infosPanel.add(featureObjIdLb);
		
		Choice featureIdChoice = new Choice();
		featureIdChoice.setBounds(100,140,100,20);
		infosPanel.add(featureIdChoice);
		
		
		Label featureTimeLb = new Label("Time step");
		featureTimeLb.setBackground(new Color(0.9f,0.9f,0.9f));
		featureTimeLb.setBounds(220, 140, 75, 20);
		infosPanel.add(featureTimeLb);
		
		TextField featureTimeTF = new TextField("0");
		featureTimeTF.setBounds(295,140,80,20);
		featureTimeTF.addKeyListener(kNumberInt);
		infosPanel.add(featureTimeTF);
		
		JPanel sfPanel = new JPanel();
		sfPanel.setLayout(new GridBagLayout());
		
		
		//scrollpane in which to display the entries:
		JScrollPane scrollFeatures = new JScrollPane(sfPanel,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollFeatures.setBounds(0,200,375,200);
		scrollFeatures.setPreferredSize(new Dimension(375,500));
		infosPanel.add(scrollFeatures);
		
		
		ResultsTable tb=ResultsTable.getResultsTable();//default table and headings
		String[] headings = tb.getHeadings();
		
		if(resultsChoice.getSelectedItem()!=null) {//if we find another results table
			if(ResultsTable.getResultsTable(resultsChoice.getSelectedItem())!=null) {
				tb=ResultsTable.getResultsTable(resultsChoice.getSelectedItem());//default table and headings
				headings = tb.getHeadings();
			}
		}
		
		
		
		
		
		for(String s : headings) {
			featureIdChoice.add(s);
		}
		
		
		
		//build features scrollBar
		ClearAndBuildResultsScroll(headings,sfPanel,scrollFeatures);
		
		seeDatasetInMN = new Button("See on MorphoNet");
		seeDatasetInMN.setBackground(new Color(0.0f,0.78f,0.33f));
		seeDatasetInMN.setBounds(380, 160, 170, 40);
		seeDatasetInMN.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				SeeDatasetOnMN();
			}
		});
		infosPanel.add(seeDatasetInMN);
		seeDatasetInMN.setVisible(false);
		
		UploadFromFiji = new Button("Upload from Results");
		UploadFromFiji.setBackground(new Color(0.26f,0.83f,1.0f));
		UploadFromFiji.setBounds(380, 410, 170, 40);
		UploadFromFiji.setEnabled(false);
		infosPanel.add(UploadFromFiji);
		
		UploadFromFiji.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				try {
					UploadFromFiji.setEnabled(false);
					Component[]children =  sfPanel.getComponents();//loop over all panels, and get info
					int fid=0;
					for(Component c : children) {
						if(c.getClass().getName().equals("javax.swing.JPanel")) {
							JPanel p = (JPanel)c;
							Checkbox cb = (Checkbox)p.getComponent(0);
							Label lb = (Label)p.getComponent(1);
							Choice ty = (Choice)p.getComponent(2);
							System.out.println(cb.getState()+","+lb.getText()+","+ty.getSelectedItem());
							if(cb.getState()) {
								UploadFeatureFromResults(featureIdChoice.getSelectedIndex(),fid,featureTimeTF.getText(),ty.getSelectedItem(),PickDatasetField.getText(),resultsChoice.getSelectedItem());
							}
							
						}
						fid++;
					}
					seeDatasetInMN.setBounds(180, 410, 170, 40);
					seeDatasetInMN.setVisible(true);
				} catch (IOException e) {
					IJ.error("Error : UploadFeatureFromResults : "+e);
				}
				
			}
		});
		
		featuresMissingWarningLb = new Label("Results are empty!");
		featuresMissingWarningLb.setForeground(Color.RED);
		featuresMissingWarningLb.setBounds(400,142,130,30);
		infosPanel.add(featuresMissingWarningLb);
		featuresMissingWarningLb.setVisible(false);
		if(tb.getHeadings().length<=0) {
			featuresMissingWarningLb.setVisible(true);
		}
		
		Button RefreshResults = new Button("Refresh Results");
		RefreshResults.setBounds(400, 110, 130, 30);
		infosPanel.add(RefreshResults);
		RefreshResults.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {//open up side GUI for picking
				ResultsTable ntb=ResultsTable.getResultsTable();
				String[] headings = ntb.getHeadings();
				
				featureIdChoice.removeAll();
				resultsChoice.removeAll();
				for(String s : WindowManager.getNonImageTitles()) {//refresh results choice
					resultsChoice.add(s);
				}
				
				if(resultsChoice.getSelectedItem()!=null) {//if we find another results table
					if(ResultsTable.getResultsTable(resultsChoice.getSelectedItem())!=null) {
						ntb=ResultsTable.getResultsTable(resultsChoice.getSelectedItem());//default table and headings
						headings = ntb.getHeadings();
					}
				}
				
				ClearAndBuildResultsScroll(ntb.getHeadings(),sfPanel,scrollFeatures);
				infosPanel.add(scrollFeatures);
				if(ntb.getHeadings().length>0) {
					
					
					
					featureIdChoice.removeAll();
					for(String s : headings) {
						featureIdChoice.add(s);
					}
					
					featuresMissingWarningLb.setVisible(false);
					if(!PickDatasetField.getText().equals("Empty")) {
						UploadFromFiji.setEnabled(true);
					}
				}else {
					UploadFromFiji.setEnabled(false);
					featuresMissingWarningLb.setVisible(true);
				}
				
				
			}
		});
		
		//add listener on results choice too !
		resultsChoice.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) { //on changing item, update feature array and choice(s)
				ResultsTable ntb=ResultsTable.getResultsTable();
				String[] headings = ntb.getHeadings();
				featureIdChoice.removeAll();
				
				
				if(resultsChoice.getSelectedItem()!=null) {//if we find another results table
					if(ResultsTable.getResultsTable(resultsChoice.getSelectedItem())!=null) {
						ntb=ResultsTable.getResultsTable(resultsChoice.getSelectedItem());//default table and headings
						headings = ntb.getHeadings();
					}
				}
				
				ClearAndBuildResultsScroll(ntb.getHeadings(),sfPanel,scrollFeatures);
				infosPanel.add(scrollFeatures);
				if(ntb.getHeadings().length>0) {
					
					
					
					featureIdChoice.removeAll();
					for(String s : headings) {
						featureIdChoice.add(s);
					}
					
					
					featuresMissingWarningLb.setVisible(false);
					if(!PickDatasetField.getText().equals("Empty")) {
						UploadFromFiji.setEnabled(true);
					}
				}else {
					UploadFromFiji.setEnabled(false);
					featuresMissingWarningLb.setVisible(true);
				}
			}
		});
		
		
		/**----PART TO UPLOAD FROM FILE----**/
		//line between 2 parts
		JPanel fline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		fline.setBounds(0, 470, 550, 1);
		infosPanel.add(fline);
		
		JFileChooser featureFileChooser = new JFileChooser();
		featureFileChooser.setFileFilter(new FileNameExtensionFilter("Text files (.txt)", "txt"));
		featureFileChooser.setAcceptAllFileFilterUsed(false);
		
		Label featureFileLb = new Label("Feature file");
		featureFileLb.setBackground(new Color(0.9f,0.9f,0.9f));
		featureFileLb.setBounds(0, 490, 75, 20);
		infosPanel.add(featureFileLb);
		
		featureFilePath = new TextField("");
		featureFilePath.setEditable(false);
		featureFilePath.setBounds(100,490,280,22);
		infosPanel.add(featureFilePath);
		
		Button SelectFile = new Button("Select file");
		SelectFile.setBounds(400, 485, 130, 30);
		infosPanel.add(SelectFile);
		
		
		Label featureFileErrorLb = new Label("Needs a file!");
		featureFileErrorLb.setBackground(new Color(0.9f,0.9f,0.9f));
		featureFileErrorLb.setBounds(100, 518, 120, 20);
		featureFileErrorLb.setForeground(Color.RED);
		infosPanel.add(featureFileErrorLb);
		
		UploadFromFile = new Button("Upload from File");
		UploadFromFile.setBackground(new Color(0.26f,0.83f,1.0f));
		UploadFromFile.setBounds(380, 530, 170, 40);
		UploadFromFile.setEnabled(false);
		infosPanel.add(UploadFromFile);
		UploadFromFile.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				try {
					UploadFeatureFromFile(featureFilePath.getText());
					seeDatasetInMN.setBounds(180, 530, 170, 40);
					seeDatasetInMN.setVisible(true);
				} catch (IOException e) {
					System.out.println("Error UploadFeatureFromFile : "+e);
				}
				
			}
		});
		
		
		SelectFile.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				if(featureFileChooser.showSaveDialog(null)==JFileChooser.APPROVE_OPTION) {
					featureFilePath.setText(featureFileChooser.getSelectedFile().getAbsolutePath());
					if(PickDatasetField.getText()!="Empty")
						UploadFromFile.setEnabled(true);
					featureFileErrorLb.setVisible(false);
				}
				
			}
		});
		
	}
	
	/**
	 * special builder function : clean and build the currently selected results features array
	 * @param headings array containing the headings of the result array from which to build the UI
	 * @param sfPanel panel to update
	 * @param scrollFeatures scroll pane to update
	 */
	public static void ClearAndBuildResultsScroll(String[] headings, JPanel sfPanel,JScrollPane scrollFeatures) {
		
		sfPanel.removeAll();
		
		String []types=new String[5];
		types[0]="float";
		types[1]="string";
		types[2]="selection";
		types[3]="group";
		types[4]="time";
		
		GridBagConstraints lc = new GridBagConstraints();
		lc.fill = GridBagConstraints.HORIZONTAL;
		lc.gridwidth=1;
		lc.weightx = 1;
		lc.weighty=0.5;
		lc.gridx = 0;
		lc.gridy = 0;
		lc.insets = new Insets(0,0,0,0);
		lc.anchor = GridBagConstraints.NORTHWEST;
		
		int i=0;
		for(String s : headings) {
			
			lc.gridx = 0;
			lc.gridy = i;
			JPanel tmpPanel = new JPanel();
			tmpPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, Color.black));
			tmpPanel.setLayout(new GridBagLayout());
			GridBagConstraints pc = new GridBagConstraints();
			pc.fill = GridBagConstraints.VERTICAL;
			pc.insets = new Insets(1,2,1,2);
			pc.gridheight = 1;
			pc.gridwidth = 1;
			pc.weightx = 1;
			pc.weighty = 1;
			pc.gridx = 0;
			pc.gridy = 0;
			pc.anchor = GridBagConstraints.WEST;
			Checkbox tmpck = new Checkbox();
			tmpck.setState(true);
			if(i==0) {
				tmpck.setState(false);
			}
			tmpPanel.add(tmpck,pc);
			pc.anchor = GridBagConstraints.WEST;
			pc.gridx = 1;
			Label tmpLabel = new Label(s);
			tmpLabel.setPreferredSize(new Dimension(150,10));
			tmpPanel.add(tmpLabel,pc);
			pc.gridx = 2;
			pc.anchor = GridBagConstraints.EAST;
					Choice tmpChoice = new Choice();
			for(String ts : types) {
				tmpChoice.add(ts);
			}
			tmpChoice.setPreferredSize(new Dimension(100,10));
			tmpPanel.add(tmpChoice,pc);
			
			sfPanel.add(tmpPanel,lc);
			
			tmpPanel.validate();
			tmpPanel.repaint();
			
			i++;
		}
		
		sfPanel.validate();
		sfPanel.repaint();
		
		scrollFeatures.validate();
		scrollFeatures.repaint();
		
		infosPanel.validate();
		infosPanel.repaint();
	}
	
	
	/**
	 * Main MorphoNet Plugin menu : This interface opens all other menus
	 */
	public static void PluginMenu() {
		
		if(pm!=null) {
			IJ.error("please close other instances of the MorphoNetJ main menu");
			return;
		}
		
		trustAllHosts();
		
		//check if current version is up-to-date : 
		String lastVersion = GetCurrentUpToDateVersion();
		if(lastVersion!="") {
			if(!currentVersion.contentEquals(lastVersion)) {
				//IJ.showMessage("Newer version detected online, please update the plugin by clicking on the Help > Update... option");
				upToDate=false;
			}
		}
		
		
		Frame pm = new Frame("MorphoNetJ (v"+currentVersion+")");//main menu frame
		pm.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				pm.dispose();
				System.out.println("Closed Morphonet main menu");
				//pm=null;
			}
		});
		pm.setResizable(false);
		pm.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		Font mainMenuFont = new Font("Verdana", Font.BOLD,14);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth=1;
		c.weightx = 1;
		c.weighty=0.5;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(20,20,20,20);
		c.anchor = GridBagConstraints.NORTHWEST;
		JTextArea introLb = new JTextArea("Morphonet is an interactive anatomical browser for 3D, 3D+t segmented datasets. It provides a comprehensive palette of interaction tools to explore the structure, dynamics and variability of biological shapes, from fossils to developing embryos. This plugin allows you to Upload data to MorphoNet from segmented images, and add information to your datasets.");
		introLb.setLineWrap(true);
		introLb.setEditable(false);
		introLb.setWrapStyleWord(true);
		introLb.setBackground(new Color(0.9f,0.9f,0.9f));
		//introLb.setFont(mainMenuFont);
		pm.add(introLb,c);
		
		//morphoNet label
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth=1;
		c.gridheight=1;
		c.weightx = 1;
		c.weighty=0.5;
		c.gridx = 1;
		c.gridy = 0;
		c.insets = new Insets(10,10,10,10);
		c.anchor = GridBagConstraints.NORTHWEST;
		
		try {
			JLabel picLogo = new JLabel(new ImageIcon(getImage("images/LogoTransparentWithShadow.png")));
			pm.add(picLogo,c);
		} catch (Exception e1) {
			System.out.println("error : could not load MorphoNet logo");
		}
		
		Map<TextAttribute, Integer> fontAttributes = new HashMap<TextAttribute, Integer>();
		fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		Font linkFont = new Font("Verdana", Font.PLAIN,14).deriveFont(fontAttributes);
		
		//line just for the username/logged in part
		c.gridwidth=1;
		c.gridheight=1;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(20,20,20,20);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		JTextArea userLoggedText = new JTextArea("Not currently logged in");
		userLoggedText.setLineWrap(true);
		userLoggedText.setEditable(false);
		userLoggedText.setWrapStyleWord(true);
		userLoggedText.setFont(mainMenuFont);
		userLoggedText.setBackground(new Color(0.9f,0.9f,0.9f));
		pm.add(userLoggedText,c);
		
		
		
		
		c.gridwidth=1;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(20,20,20,20);
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weightx = 0.5;
		Button MNBtn = new Button("MorphoNet.org");
		MNBtn.setFont(linkFont);
		MNBtn.setForeground(new Color(0.26f,0.83f,1.0f));
		pm.add(MNBtn,c);
		
		
		//login menu
		c.gridwidth=1;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 2;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		
		JTextArea loginText = new JTextArea("Most of the features of this plugin require you to be logged in with a MorphoNet account");
		loginText.setLineWrap(true);
		loginText.setEditable(false);
		loginText.setWrapStyleWord(true);
		loginText.setBackground(new Color(0.9f,0.9f,0.9f));
		pm.add(loginText,c);
		
		
		c.gridwidth=1;
		c.gridx = 1;
		c.gridy = 2;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.insets = new Insets(5,20,5,20);
		c.weightx = 0.5;
		
		Panel LogPanel = new Panel();
		LogPanel.setLayout(new GridBagLayout());
		LogPanel.setBackground(new Color(0.9f,0.9f,0.9f));
		pm.add(LogPanel,c);
		
		c.gridwidth=1;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(5,0,5,0);
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weightx = 0.5;
		Button LoginBtn = new Button("Log in");
		LogPanel.add(LoginBtn,c);
		
		c.gridwidth=1;
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weightx = 0.5;
		Button SignInBtn = new Button("Create account");
		LogPanel.add(SignInBtn,c);
		
		
		/**----Line for the Convert and upload plugin**/
		c.gridwidth=1;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 3;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(20,20,20,20);
		
		JTextArea textCAndU = new JTextArea("If you want to quickly convert segmented 3D or 4D images to meshes and Upload them on MorphoNet to view them, use this plugin.");
		textCAndU.setLineWrap(true);
		textCAndU.setWrapStyleWord(true);
		textCAndU.setBackground(new Color(0.9f,0.9f,0.9f));
		textCAndU.setEditable(false);
		pm.add(textCAndU,c);
		
		c.gridwidth=1;
		c.gridx = 1;
		c.gridy = 3;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weightx = 0.5;
		
		Button CUBtn = new Button("Convert and Upload");
		CUBtn.setEnabled(false);
		CUBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				MainMenu();
			}
		});
		pm.add(CUBtn,c);
		
		/**----Line for the Convert plugin**/
		c.gridwidth=1;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 4;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		
		JTextArea textConvert = new JTextArea("If you want to convert segmented 3D or 4D images into .obj meshes and save them locally, use this plugin.");
		textConvert.setLineWrap(true);
		textConvert.setWrapStyleWord(true);
		textConvert.setBackground(new Color(0.9f,0.9f,0.9f));
		textConvert.setEditable(false);
		pm.add(textConvert,c);
		
		c.gridwidth=1;
		c.gridx = 1;
		c.gridy = 4;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weightx = 0.5;
		
		Button ConvertBtn = new Button("Convert");
		ConvertBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				ConvertMenu();
			}
		});
		pm.add(ConvertBtn,c);
		
		/**----Line for the Upload meshes plugin**/
		c.gridwidth=1;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 5;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		
		JTextArea textUpload = new JTextArea("If you want to upload .obj meshes to datasets and/or create new datasets with available meshes, use this plugin.");
		textUpload.setLineWrap(true);
		textUpload.setWrapStyleWord(true);
		textUpload.setBackground(new Color(0.9f,0.9f,0.9f));
		textUpload.setEditable(false);
		pm.add(textUpload,c);
		
		c.gridwidth=1;
		c.gridx = 1;
		c.gridy = 5;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weightx = 0.5;
		
		Button UploadBtn = new Button("Upload .obj meshes");
		UploadBtn.setEnabled(false);
		UploadBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				UploadMenu();
			}
		});
		pm.add(UploadBtn,c);
		
		
		/**----Line for the Upload images plugin**/
		c.gridwidth=1;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 6;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		
		JTextArea textUploadImages = new JTextArea("If you want to upload 8-bit greyscale 3D images to MorphoNet Datasets, use this plugin.");
		textUploadImages.setLineWrap(true);
		textUploadImages.setWrapStyleWord(true);
		textUploadImages.setBackground(new Color(0.9f,0.9f,0.9f));
		textUploadImages.setEditable(false);
		pm.add(textUploadImages,c);
		
		c.gridwidth=1;
		c.gridx = 1;
		c.gridy = 6;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weightx = 0.5;
		
		Button UploadImgBtn = new Button("Upload Raw Images");
		UploadImgBtn.setEnabled(false);
		UploadImgBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				ImagesMenu();
			}
		});
		pm.add(UploadImgBtn,c);
		
		/**----Line for the Upload Features plugin**/
		c.gridwidth=1;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 7;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		
		JTextArea textUploadFeatures = new JTextArea("If you want to upload Features to MorphoNet Datasets, use this plugin. For instance, Features could be cell names, volumes, or other, and can easily be computed with the MorphoLibJ plugin.");
		textUploadFeatures.setLineWrap(true);
		textUploadFeatures.setWrapStyleWord(true);
		textUploadFeatures.setBackground(new Color(0.9f,0.9f,0.9f));
		textUploadFeatures.setEditable(false);
		pm.add(textUploadFeatures,c);
		
		c.gridwidth=1;
		c.gridx = 1;
		c.gridy = 7;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.weightx = 0.5;
		
		Button UploadFeaturesBtn = new Button("Upload Features");
		UploadFeaturesBtn.setEnabled(false);
		UploadFeaturesBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				FeaturesMenu();
			}
		});
		pm.add(UploadFeaturesBtn,c);
		
		
		/**----Line for the updates message**/
		c.gridwidth=2;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 8;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		c.insets = new Insets(10,20,10,20);
		
		JTextArea updateText = new JTextArea("");
		
		if(!upToDate) {
			updateText.setText("v"+currentVersion+" - Update available : please update with the Help > Update... option");
			updateText.setFont(mainMenuFont);
		}else {
			updateText.setText("v"+currentVersion+" - Up to date");
		}
		
		updateText.setLineWrap(true);
		updateText.setWrapStyleWord(true);
		updateText.setBackground(new Color(0.9f,0.9f,0.9f));
		updateText.setEditable(false);
		pm.add(updateText,c);
		
		//connect button
		LoginBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				if(!connected) {
					connected=Connect();
					if(connected) {
						UploadFeaturesBtn.setEnabled(true);
						UploadImgBtn.setEnabled(true);
						UploadBtn.setEnabled(true);
						CUBtn.setEnabled(true);
						userLoggedText.setText("Logged in as "+login+"");
						pm.setName("MorphoNetJ (logged in as "+login+") (v"+currentVersion+")");
						SignInBtn.setEnabled(false);
						LoginBtn.setLabel("Disconnect");
					}
				}else {//disconnect mode
					connected=false;
					login="";
					token="";
					UploadFeaturesBtn.setEnabled(false);
					UploadImgBtn.setEnabled(false);
					UploadBtn.setEnabled(false);
					CUBtn.setEnabled(false);
					userLoggedText.setText("Not currently logged in");
					pm.setName("MorphoNetJ (v"+currentVersion+")");
					SignInBtn.setEnabled(true);
					LoginBtn.setLabel("Log in");
				}
			}
				
		});
		
		SignInBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				CreateMNAccount();
			}
		});
		
		MNBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {
				GoToMorphoNet();
			}
		});
		
		
		pm.setSize(650,820); //650,500 is the base value
		pm.setVisible(true);
		pm.setBackground(new Color(0.9f,0.9f,0.9f));
	}
	
	
	
	/**
	 * Launches the convert and upload sub-plugin
	 */
	public static void MainMenu() {

		if(mm!=null) {
			IJ.error("please close other instances of MorphoNetJ Convert and Upload plugin");
			return;
		}
		
		
		
		trustAllHosts();
		
		String osMorphoDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"MorphoNet"+File.separator;
		if(System.getProperty("os.name").toLowerCase().contains("mac")) {//change the path if we are on mac
			osMorphoDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop"+File.separator+"MorphoNet"+File.separator;
		}
		if(System.getProperty("os.name").toLowerCase().contains("linux")) {//change the path if we are on mac
			osMorphoDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop"+File.separator+"MorphoNet"+File.separator;
		}
		
		
		//connect first thing:
		try {
			if(!IsTokenValid())
				connected=Connect();
		} catch (NoSuchAlgorithmException | IOException e1) {
			System.out.println("Error : could not connect to MorphoNet server");
		}
		if(!connected) {
			return;
		}
		
				
		
		
		//if it does not exists, create a morphoNet folder on the user desktop
		File morphoDir = new File(osMorphoDir);
		if(!morphoDir.exists() && !morphoDir.isDirectory()) {
			if(!morphoDir.mkdir()) {
				IJ.error("ERROR : could not create MorphoNet default directory for output ! Path tried : "+morphoDir.getAbsolutePath());
			}
		}
		defaultDirPath = osMorphoDir;
		

		
		mm = new Frame("MorphoNet Convert and Upload Plugin (logged in as "+login+")");//main menu frame
		mm.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				mm.dispose();
				System.out.println("Closed MorphonetJ");
				mm=null;
			}
		});
		mm.setResizable(false);

		mainPartFont = new Font("Verdana", Font.BOLD,14);

		Label introLabel = new Label("This plugin converts 3D/4D images to meshes and uploads them to MorphoNet");
		introLabel.setBackground(new Color(0.9f,0.9f,0.9f));
		introLabel.setBounds(50, 50, 550, 30);
		mm.add(introLabel);
		
		//line between 2 parts
		JPanel bgline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		bgline.setBounds(50, 80, 550, 1);
		mm.add(bgline);
		
		
		/**Convert part**/
		BuildConvertMenu(100,true);
		
		//init convert bar, but hide it :
		ConvertProgress = new JProgressBar(0,1);
		ConvertProgress.setBounds(75, 330, 500, 20);
		ConvertProgress.setString("Converting data ...");
		mm.add(ConvertProgress);
		ConvertProgress.setVisible(false);
		
		
		//line between 2 parts
		JPanel cmline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		cmline.setBounds(50, 360, 550, 1);
		mm.add(cmline);

		/**MESH PATH PART : built but hidden by upload menu for simplicity**/
		BuildMeshPathMenu(370,true,true);
		MeshPanel.setVisible(false);
		BuildUploadMenu(370,false);

		


		/**UPLOAD TO MORPHONET PART**/

		
		//init convert bar, but hide it :
		UploadProgress = new JProgressBar(0,1);
		UploadProgress.setBounds(25, 130, 500, 20);
		UploadProgress.setString("Uploading data ...");
		uploadPanel.add(UploadProgress);
		UploadProgress.setVisible(false);

		//Convert Button : is down here because of needed parameters
		//launch conversion button
		convertBtn = new Button("Convert & Upload");
		convertBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {

				new Thread(new Runnable() {
					@Override
					public void run() {

						if(Integer.valueOf(maxTSTF.getText())-Integer.valueOf(minTSTF.getText())<500) { //check that the user is not uploading 500 time steps or more at once
								ComputeMeshesClassic(WindowManager.getImage(imgChoice.getSelectedItem()),filePathTF.getText(),
									Integer.valueOf(minTSTF.getText()), Integer.valueOf(maxTSTF.getText()), factorScaleSlider.getValue(),ZfactorScaleSlider.getValue(),
									Integer.valueOf(bkgVal.getText()), Float.valueOf(vXvalue.getText()),Float.valueOf(vYvalue.getText()),Float.valueOf(vZvalue.getText()),
									Integer.valueOf(minVoxVal.getText()),borderBox.getState(),smoothBox.getState(), Float.valueOf(spVal.getText()), Integer.valueOf(siVal.getText()),
									QCBox.getState(),Float.valueOf(ndVal.getText()), DBox.getState(), Float.valueOf(drVal.getText()), Integer.valueOf(verboseValue.getSelectedItem()),
									ViewerBox.getState(),true,channelTF.getText(), Integer.valueOf(autoDecimateThresholdTF.getText()),splitMeshActivate.getState(), Integer.valueOf(splitMeshThreshold.getText()));
						}else {
							IJ.error("Error : cannot convert that many time steps at once (max 500). It is recommended to avoid putting over 500 time steps in a dataset, even in several uploads.");
						}
					}
				}).start();

			}
		});
		
		convertBtn.setBounds(390, 160, 160, 40);
		if(imgChoice.getSelectedItem()==null)
			if(datasetToUpdate.getText()=="")
				convertBtn.setEnabled(false);//disabled by default, gets enabled if an image is selected and a path is set
		convertBtn.setBackground(new Color(0.26f,0.83f,1.0f));
		uploadPanel.add(convertBtn);




		mm.setSize(650,600); //650,500 is the base value
		mm.setLayout(null);
		mm.setVisible(true);
		mm.setBackground(new Color(0.9f,0.9f,0.9f));


	}


	/**
	 * Launches the Convert sub-plugin
	 */
	public static void ConvertMenu() {
		
		if(mm!=null) {
			IJ.error("please close other instances of MorphoNetJ Convert plugin");
			return;
		}
		
		
		String osMorphoDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"MorphoNet"+File.separator;
		if(System.getProperty("os.name").toLowerCase().contains("mac")) {//change the path if we are on mac
			osMorphoDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop"+File.separator+"MorphoNet"+File.separator;
		}
		if(System.getProperty("os.name").toLowerCase().contains("linux")) {//change the path if we are on mac
			osMorphoDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop"+File.separator+"MorphoNet"+File.separator;
		}
		
		//if it does not exists, create a morphoNet folder on the user desktop
		File morphoDir = new File(osMorphoDir);
		if(!morphoDir.exists() && !morphoDir.isDirectory()) {
			if(!morphoDir.mkdir()) {
				IJ.error("ERROR : could not create MorphoNet default directory for output ! Path tried : "+morphoDir.getAbsolutePath());
			}
		}
		defaultDirPath = osMorphoDir;
		

		
		mm = new Frame("MorphoNet Convert Plugin");//main menu frame
		mm.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				mm.dispose();
				System.out.println("Closed MorphonetJ");
				mm=null;
			}
		});
		mm.setResizable(false);

		mainPartFont = new Font("Verdana", Font.BOLD,14);

		Label introLabel = new Label("This plugin allows you to convert 3D/4D images to .obj meshes");
		introLabel.setBackground(new Color(0.9f,0.9f,0.9f));
		introLabel.setBounds(50, 50, 550, 30);
		mm.add(introLabel);
		
		//line between 2 parts
		JPanel bgline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		bgline.setBounds(50, 80, 550, 1);
		mm.add(bgline);
		
		
		/**Convert part**/
		BuildConvertMenu(100,false);

		//line between 2 parts
		JPanel cmline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		cmline.setBounds(50, 310, 550, 1);
		mm.add(cmline);

		/**MESH PATH PART**/
		BuildMeshPathMenu(340,true,false);
		
		ConvertProgress = new JProgressBar(0,1);
		ConvertProgress.setBounds(75, 480, 500, 20);
		ConvertProgress.setString("Converting data ...");
		mm.add(ConvertProgress);
		ConvertProgress.setVisible(false);
		
		//Convert Button : is down here because of needed parameters
		//launch conversion button
		convertBtn = new Button("Convert");
		convertBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {

				new Thread(new Runnable() {
					@Override
					public void run() {
							ComputeMeshesClassic(WindowManager.getImage(imgChoice.getSelectedItem()),filePathTF.getText(),
								Integer.valueOf(minTSTF.getText()), Integer.valueOf(maxTSTF.getText()), factorScaleSlider.getValue(),ZfactorScaleSlider.getValue(),
								Integer.valueOf(bkgVal.getText()), Float.valueOf(vXvalue.getText()),Float.valueOf(vYvalue.getText()),Float.valueOf(vZvalue.getText()),
								Integer.valueOf(minVoxVal.getText()),borderBox.getState(),smoothBox.getState(), Float.valueOf(spVal.getText()), Integer.valueOf(siVal.getText()),
								QCBox.getState(),Float.valueOf(ndVal.getText()), DBox.getState(), Float.valueOf(drVal.getText()), Integer.valueOf(verboseValue.getSelectedItem()),
								ViewerBox.getState(),false,channelTF.getText(),Integer.valueOf(autoDecimateThresholdTF.getText()),splitMeshActivate.getState(), Integer.valueOf(splitMeshThreshold.getText()));
					}
				}).start();

			}
		});
		
		convertBtn.setBounds(450, 510, 130, 40);
		if(imgChoice.getSelectedItem()==null)
			convertBtn.setEnabled(false);//disabled by default, gets enabled if an image is selected and a path is set
		convertBtn.setBackground(new Color(0.26f,0.83f,1.0f));
		mm.add(convertBtn);
		
		mm.setSize(650,600); //650,500 is the base value
		mm.setLayout(null);
		mm.setVisible(true);
		mm.setBackground(new Color(0.9f,0.9f,0.9f));		
		
	}
	
	/**
	 * Launch the Upload dataset sub-menu
	 */
	public static void UploadMenu() {
		
		if(mm!=null) {
			IJ.error("please close other instances of MorphoNetJ Upload plugin");
			return;
		}
		
		
		trustAllHosts();
		
		String osMorphoDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"MorphoNet"+File.separator;
		if(System.getProperty("os.name").toLowerCase().contains("mac")) {//change the path if we are on mac
			osMorphoDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop"+File.separator+"MorphoNet"+File.separator;
		}
		if(System.getProperty("os.name").toLowerCase().contains("linux")) {//change the path if we are on mac
			osMorphoDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().toString()+File.separator+"Desktop"+File.separator+"MorphoNet"+File.separator;
		}
		
		
		//connect first thing:
		try {
			if(!IsTokenValid())
				connected=Connect();
		} catch (NoSuchAlgorithmException | IOException e1) {
			System.out.println("Error : could not connect to MorphoNet server");
		}
		if(!connected) {
			return;
		}

		

		defaultDirPath = osMorphoDir;
		
		
		mm = new Frame("MorphoNet Upload Plugin (logged in as "+login+")");//main menu frame
		mm.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				mm.dispose();
				System.out.println("Closed MorphonetJ");
				mm=null;
			}
		});
		mm.setResizable(false);

		mainPartFont = new Font("Verdana", Font.BOLD,14);

		Label introLabel = new Label("This plugin allows you to upload .obj meshes to MorphoNet datasets");
		introLabel.setBackground(new Color(0.9f,0.9f,0.9f));
		introLabel.setBounds(50, 50, 550, 30);
		mm.add(introLabel);
		
		//line between 2 parts
		JPanel bgline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		bgline.setBounds(50, 80, 550, 1);
		mm.add(bgline);
		
		/**MESH PATH PART**/
		BuildMeshPathMenu(110,false,true);

		
		//line between 2 parts
		JPanel muline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		muline.setBounds(50, 205, 550, 1);
		mm.add(muline);

		/**UPLOAD TO MORPHONET PART**/
		BuildUploadMenu(230,true);
		
		UploadProgress = new JProgressBar(0,1);
		UploadProgress.setBounds(25, 125, 500, 20);
		UploadProgress.setString("Uploading data ...");
		uploadPanel.add(UploadProgress);
		UploadProgress.setVisible(false);
		
		
		mm.setSize(650,500); //650,500 is the base value
		mm.setLayout(null);
		mm.setVisible(true);
		mm.setBackground(new Color(0.9f,0.9f,0.9f));

		
	}
	
	/**
	 * Launch Upload RAW images subplugin
	 */
	public static void ImagesMenu() {
		
		if(mm!=null) {
			IJ.error("please close other instances of MorphoNetJ Raw Images plugin");
			return;
		}
		
		
		trustAllHosts();
		
		
		//connect first thing:
		try {
			if(!IsTokenValid())
				connected=Connect();
		} catch (NoSuchAlgorithmException | IOException e1) {
			System.out.println("Error : could not connect to MorphoNet server");
		}
		if(!connected) {
			return;
		}
		
		
		mm = new Frame("MorphoNet Upload Images Plugin (logged in as "+login+")");//main menu frame
		mm.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				mm.dispose();
				System.out.println("Closed MorphonetJ");
				mm=null;
			}
		});
		mm.setResizable(false);

		mainPartFont = new Font("Verdana", Font.BOLD,14);

		Label introLabel = new Label("This plugin allows you to upload 3D images to MorphoNet datasets");
		introLabel.setBackground(new Color(0.9f,0.9f,0.9f));
		introLabel.setBounds(50, 50, 550, 30);
		mm.add(introLabel);
		
		//line between 2 parts
		JPanel bgline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		bgline.setBounds(50, 80, 550, 1);
		mm.add(bgline);
		
		ImageProgress = new JProgressBar(0,1);
		ImageProgress.setBounds(75, 500, 500, 20);
		ImageProgress.setString("Converting data ...");
		mm.add(ImageProgress);
		ImageProgress.setVisible(false);
		
		BuildImagesMenu(100,true,true,true);
		
		
		
		
		mm.setSize(650,550); //650,500 is the base value
		mm.setLayout(null);
		mm.setVisible(true);
		mm.setBackground(new Color(0.9f,0.9f,0.9f));
	}
	
	/**
	 * Launch Upload Features subplugin
	 */
	public static void FeaturesMenu() {
		
		
		if(mm!=null) {
			IJ.error("please close other instances of MorphoNetJ Features plugin");
			return;
		}
		
		
		trustAllHosts();
		
		
		//connect first thing:
		try {
			if(!IsTokenValid())
				connected=Connect();
		} catch (NoSuchAlgorithmException | IOException e1) {
			System.out.println("Error : could not connect to MorphoNet server");
		}
		if(!connected) {
			return;
		}
		
		
		mm = new Frame("MorphoNet Upload Features Plugin (logged in as "+login+")");//main menu frame
		mm.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				mm.dispose();
				System.out.println("Closed MorphonetJ");
				mm=null;
			}
		});
		mm.setResizable(false);

		mainPartFont = new Font("Verdana", Font.BOLD,14);

		Label introLabel = new Label("This plugin allows you to upload Fiji tables as informations to MorphoNet datasets");
		introLabel.setBackground(new Color(0.9f,0.9f,0.9f));
		introLabel.setBounds(50, 50, 550, 30);
		mm.add(introLabel);
		
		//line between 2 parts
		JPanel bgline = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setColor(new Color(0.6f,0.6f,0.6f));
				g2.draw(new java.awt.geom.Line2D.Double(0, 0, 550, 0));
			}
		};
		bgline.setBounds(50, 80, 550, 1);
		mm.add(bgline);
		
		BuildFeaturesMenu(100,true,true);
		
		
		
		
		mm.setSize(650,700); //650,500 is the base value
		mm.setLayout(null);
		mm.setVisible(true);
		mm.setBackground(new Color(0.9f,0.9f,0.9f));
		
		
	}
	
	
	
	/**
	 * with the standard naming convention, get the time step from a created obj file
	 * @param path path of the file
	 */
	public static String GetTimeStepFromMeshName(String path) {
		String name = path.split(".obj")[0];
		String[] ts = name.split("t");
		return ts[ts.length-1];
		
	}
	

	/**
	 * Adds .obj at the end of a path if it does not end with it
	 * @param path path of the file
	 */
	public static String addObjExtensionToPath(String path) {
		if(!path.endsWith(".obj") ) {
			path = path+".obj";
		}
		return path;
	}
	

	/**
	 * Update the label with the number of found meshes in current path
	 * @param path path of the file/directory
	 */
	public static void UpdateNumberOfFoundMeshesOnPath(String path) {
		if(foundMeshesLabel!=null) {
			try {
				int meshNb = checkObjMeshesInPath(path);
				foundMeshesLabel.setText(meshNb+" meshes found in directory");
				if(meshNb>0) {
					foundMeshesLabel.setForeground(new Color(0.05f,0.5f,0.05f));
				}else {
					foundMeshesLabel.setForeground(Color.RED);
				}
			} catch (IOException e) {
				System.out.println("checkObjMeshesInPath Error : "+e);
			}
		}
		if(pathToUploadTF!=null) {
			pathToUploadTF.setText(removeFileFromPath(path));
		}

	}
	
	/**
	 * Updates label for number of meshes in path warning, and returns false if no meshes found
	 * @param path path of the file
	 * @param lb label to update
	 */
	public static boolean UpdateNumberOfFoundMeshesOnPath(String path, Label lb) {
		boolean ret=false;
		if(lb!=null) {
			try {
				int meshNb = checkObjMeshesInPath(path);
				lb.setText(meshNb+" meshes will be uploaded");
				if(meshNb>0) {
					lb.setForeground(new Color(0.05f,0.5f,0.05f));
					ret=true;
					if(uploadToMNBtn!=null) {
						if(datasetToUpdate!=null) {
							if(!datasetToUpdate.getText().equals(""))
								uploadToMNBtn.setEnabled(true);
						}
						
					}
				}else {
					lb.setForeground(Color.RED);
					ret=false;
					if(uploadToMNBtn!=null) {
						uploadToMNBtn.setEnabled(false);
					}
				}
			} catch (IOException e) {
				System.out.println("checkObjMeshesInPath Error : "+e);
			}
		}
		
		return ret;
		
	}

	/**
	 * check if file exists in path
	 * @param path path of the file
	 * @param fileName name of file
	 */
	public static boolean ConfirmIfObjAlreadyExistsInPath(String path, String fileName) {
		String cleanedPath = path;
		if(!new File(path).isDirectory()) {
			cleanedPath = removeFileFromPath(path);
		}
		for(File f : new File(cleanedPath).listFiles()) {
			if(removeTimeFromObjFileName(f.getName()).equals(fileName)) { //case where the file already exists : put a confirm window

				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * removes time from filename : turns file_tX.obj into file.obj
	 * @param file name of the file
	 */
	public static String removeTimeFromObjFileName(String file) {
		String[] tokens = file.split("_");
		String ret="";
		if(tokens.length>0) {
			if(tokens[tokens.length-1].startsWith("t")) {
				for(int i=0;i<tokens.length-1;i++) {
					if(i>0)
						ret+="_";
					ret+=tokens[i];
				}
				ret+=".obj";
				return ret;
			}
		}
		return file;
	}
	
	/**
	 * Update number of meshes in path for alert label
	 * @param path path to check
	 */
	public static void UpdateNumberOfUploadMeshesInPath(String path) {
		if(filesUploadReminder!=null) {
			if(!new File(path).isDirectory()) {
				filesUploadReminder.setText("1 mesh will be uploaded to MorphoNet");
				uploadObjErrorLb.setVisible(false);
				if(!uploadDatasetErrorLb.isVisible()) {
					uploadToMNBtn.setEnabled(true);
				}
			}else {
				try {
					int meshNb = checkObjMeshesInPath(path);
					filesUploadReminder.setText(meshNb+" meshes will be uploaded to MorphoNet");
					if(meshNb==0) {
						//uploadObjErrorLb.setVisible(true);
						if(datasetToUpdate.getText().equals("")) {//if we do not have a valid dataset name, disable the button !
							uploadToMNBtn.setEnabled(false);
						}
					}else {
						uploadObjErrorLb.setVisible(false);
						if(!uploadDatasetErrorLb.isVisible()) {
							uploadToMNBtn.setEnabled(true);
						}
					}
				} catch (IOException e) {
					System.out.println("checkObjMeshesInPath Error : "+e);
				}
			}
			
		}
		
	}

	/**
	 * Connect to MorphoNet to get credentials (with popup window)
	 */
	public static boolean Connect(){
		//WINDOW CONNECTION
		PasswordDialog gdconnection = new PasswordDialog("MorphoNet Connection");
		gdconnection.addMessage( "Enter your MorphoNet identification :" );
		gdconnection.addStringField("login", login,20);
		gdconnection.addPasswordField("password", "",20);
		gdconnection.showDialog();
		if(gdconnection.wasCanceled())
			return false;

		login=(String)gdconnection.getNextString();
		passwd=(String)gdconnection.getNextString();


		try {
			if(!ConnectToMorphoNet()) {
				IJ.error("Cannot connect to MorphoNet, please check your ids");
			}
			else {
				passwd="";
				return true;
			}
		} catch (NoSuchAlgorithmException | IOException e) {
			System.out.println("RUN ERROR ...."+e.toString());
			IJ.error("Cannot connect to MorphoNet, please check your ids");
		}
		passwd="";
		return false;
	}

	/**
	 * Connect to MorphoNet (make the request)
	 */
	public static boolean ConnectToMorphoNet() throws IOException, NoSuchAlgorithmException {

		URL url = new URL(morphonet_URL+"rest-auth/login/");
		Map<String, Object> params = new LinkedHashMap();
		params.put("username", login);
		params.put("password", passwd);

		byte[] postDataBytes = buildURLParams(params);

		HttpsURLConnection connec = (HttpsURLConnection)url.openConnection();
		connec.setHostnameVerifier(DO_NOT_VERIFY);
		connec.setRequestMethod("POST");
		connec.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connec.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		connec.setDoOutput(true);

		connec.getOutputStream().write(postDataBytes);

		Reader in = new BufferedReader(new InputStreamReader(connec.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;

        try {
        	//dirty method ? must discard the HTML to parse JSON
            String json = output.split("\\{")[1];//take the part after the first { character, indicating JSON
            json = json.split("\\}")[0]; //cut after the end of the JSON
            json = "{"+json+"}";
            json = json.replace("&quot;", "\"");


            JSONObject o = new JSONObject(json);
            token = o.getString("key");
            //user_id = o.getInt("user");

            return true;
        }catch(Exception e){
        	System.out.println("ERROR : could not parse connection request result : "+e);
        	return false;
        }


	}
	
	/**
	* ping the server with a GetDatasets request : if we are not logged in and have a valid token, the request will fail, and we will know there is a need to connect
	 */
	public static boolean IsTokenValid() throws IOException, NoSuchAlgorithmException{

		if(token!=null) {
			if(token.equals("")) {
				return false;
			}
		}else {
			return false;
		}
		

		URL url = new URL(morphonet_URL+"api/mydataset/");

		HttpsURLConnection connec = (HttpsURLConnection)url.openConnection();
		connec.setHostnameVerifier(DO_NOT_VERIFY);
		connec.setRequestMethod("GET");
		connec.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connec.setRequestProperty("Authorization", "Token "+token);
		connec.setDoOutput(true);


        
        if(connec.getResponseCode()==200) {
        	return true;
        }
        
        return false;
        
	}
	
	
	/**
	 * Create a dataset on MorphoNet (requires a connected user)
	 * @param name name of the dataset to create	 
	 */
	public static void CreateDataset(String name)  throws IOException, NoSuchAlgorithmException {

		List<String> userDatasets = GetDatasetsList();
		if(userDatasets.contains(name.toLowerCase())) {
			return;
		}
		
		URL url = new URL(morphonet_URL+"api/createdatasetapi/");

		String jsonPOSTData = "{\"createdataset\":\""+name+"\",\"minTime\":0,\"maxTime\":0,\"serveraddress\":\"\",\"id_NCBI\":0,\"id_type\":0,\"spf\":-1,\"dt\":-1}";

		byte[] postDataBytes = jsonPOSTData.getBytes();

		HttpsURLConnection connec = (HttpsURLConnection)url.openConnection();
		connec.setHostnameVerifier(DO_NOT_VERIFY);
		connec.setRequestMethod("POST");
		connec.setRequestProperty("Content-Type", "application/json");//attention to data type!
		connec.setRequestProperty("Authorization", "Token "+token);
		connec.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		connec.setDoOutput(true);

		connec.getOutputStream().write(postDataBytes);
		Reader in = new BufferedReader(new InputStreamReader(connec.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;
        //System.out.println(":"+output+":");
		JSONObject j = new JSONObject(output);
		dataset_update_id=j.getInt("result");
		//System.out.println(dataset_update_id);

		userDatasetsNames = GetDatasetsDict();
		
	}

	/**
	 * Get the datasets for the current user (uses credentials)
	 */
	public static void GetMyDatasets() throws IOException, NoSuchAlgorithmException{

		if(!connected) {
			IJ.error("ERROR : You are not connected !");
			return;
		}

		if(token.equals("")) {
			IJ.error("ERROR : Authentication credentials were not provided");
			return;
		}

		URL url = new URL(morphonet_URL+"api/mydataset/");

		HttpsURLConnection connec = (HttpsURLConnection)url.openConnection();
		connec.setHostnameVerifier(DO_NOT_VERIFY);
		connec.setRequestMethod("GET");
		connec.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connec.setRequestProperty("Authorization", "Token "+token);
		connec.setDoOutput(true);

		Reader in = new BufferedReader(new InputStreamReader(connec.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;
        try {
        	//dirty method ? must discard the HTML to parse JSON
            String json = output.split("\\[")[1];//take the part after the first { character, indicating JSON
            json = json.split("\\]")[0]; //cut after the end of the JSON
            json = "["+json+"]";
            json = json.replace("&quot;", "\"");
            json = json.replaceAll("<a href=.*</a>", "none");


            JSONArray j = new JSONArray(json);
            String[] dataset_names = new String[j.length()];
            for(int i=0;i<j.length();i++) {
            	dataset_names[i] = ((JSONObject) j.get(i)).getString("name");
            }

            if(j.length()==0) {
            	IJ.showMessage("You currently do not have any datasets with this account, unable to pick one");
            	return;
            }

            GenericDialog gdc = new GenericDialog("Dataset picking");
            gdc.addMessage("Select one dataset :");
            gdc.addChoice("Dataset", dataset_names, dataset_names[0]);
            gdc.showDialog();
    		if(gdc.wasCanceled())
    			return;

            int d_id = gdc.getNextChoiceIndex();
            dataset_update_id = ((JSONObject)j.get(d_id)).getInt("id");
            create_dataset=false;

            if(datasetToUpdate!=null) {
            	datasetToUpdate.setText(((JSONObject) j.get(d_id)).getString("name"));
            }
            if(uploadDatasetErrorLb!=null)
            	uploadDatasetErrorLb.setVisible(false);
            if(datasetClear!=null) {
            	datasetClear.setVisible(false);
            	datasetClear.setState(false);
            }
            
            if(PickDatasetField!=null) {
            	PickDatasetField.setText(((JSONObject) j.get(d_id)).getString("name"));
            }
            if(datasetMissingLb!=null) {//remove the error for image upload alert
            	datasetMissingLb.setVisible(false);
            	//and if there is an image selected, make the button enabled
            	if(ImageImgChoice!=null) {
            		if(ImageImgChoice.getSelectedItem()!=null) {
            			UploadImageToMN.setEnabled(true);
            		}
            	}
            }
            if(uploadToMNBtn!=null) {
            	if(convertBtn==null && numberOfFilesLb.getText().substring(0, 1)=="0")//if in upload mode, make sure there are meshes to upload too
    			{
    				uploadToMNBtn.setEnabled(false);
    			}else {
    				uploadToMNBtn.setEnabled(true);
    			}
            }
            
            if(uploadDatasetErrorLb!=null && datasetClear!=null && uploadToMNBtn!=null) {
            	uploadDatasetErrorLb.setVisible(true);
    			uploadDatasetErrorLb.setText("Dataset already exists: check option below to replace time steps");
    			datasetClear.setVisible(true);
    			datasetClear.setState(true);
    			uploadToMNBtn.setEnabled(true);
            }
            
            if(featuresMissingWarningLb!=null) {
            	if(!featuresMissingWarningLb.isVisible()) {
            		if(UploadFromFiji!=null) {
                    	UploadFromFiji.setEnabled(true);
                    }
            	}
            }
            if(UploadFromFile!=null && featureFilePath!=null) {
            	if(featureFilePath.getText()!="") {
            		UploadFromFile.setEnabled(true);
            	}
            }
        }catch(Exception e){
        	System.out.println("ERROR : could not parse connection request result : "+e);
        }

	}

	
	/**
	 * Get the current user's datasets names
	 */
	public static List<String> GetDatasetsList() throws IOException{
		
		List<String> sets = new ArrayList<String>();
		
		if(!connected) {
			IJ.error("ERROR : You are not connected !");
			return sets;
		}

		if(token.equals("")) {
			IJ.error("ERROR : Authentication credentials were not provided");
			return sets;
		}

		URL url = new URL(morphonet_URL+"api/mydataset/");

		HttpsURLConnection connec = (HttpsURLConnection)url.openConnection();
		connec.setHostnameVerifier(DO_NOT_VERIFY);
		connec.setRequestMethod("GET");
		connec.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connec.setRequestProperty("Authorization", "Token "+token);
		connec.setDoOutput(true);

		Reader in = new BufferedReader(new InputStreamReader(connec.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;
        try {
        	//dirty method ? must discard the HTML to parse JSON
            String json = output.split("\\[")[1];//take the part after the first { character, indicating JSON
            json = json.split("\\]")[0]; //cut after the end of the JSON
            json = "["+json+"]";
            json = json.replace("&quot;", "\"");
            json = json.replaceAll("<a href=.*</a>", "none");


            JSONArray j = new JSONArray(json);
            String[] dataset_names = new String[j.length()];
            for(int i=0;i<j.length();i++) {
            	dataset_names[i] = ((JSONObject) j.get(i)).getString("name");
            	sets.add(((JSONObject) j.get(i)).getString("name").toLowerCase());
            }

            
            
        }catch(Exception e){
        	System.out.println("ERROR : could not parse connection request result : "+e);
        }
        return sets;
		
	}
	
	/**
	 * Get current user's dataset names and IDs
	 */
	public static HashMap<String,Integer> GetDatasetsDict() throws IOException{
		
		HashMap<String,Integer> sets = new HashMap<String,Integer>();
		
		if(!connected) {
			IJ.error("ERROR : You are not connected !");
			return sets;
		}

		if(token.equals("")) {
			IJ.error("ERROR : Authentication credentials were not provided");
			return sets;
		}

		URL url = new URL(morphonet_URL+"api/mydataset/");

		HttpsURLConnection connec = (HttpsURLConnection)url.openConnection();
		connec.setHostnameVerifier(DO_NOT_VERIFY);
		connec.setRequestMethod("GET");
		connec.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connec.setRequestProperty("Authorization", "Token "+token);
		connec.setDoOutput(true);

		Reader in = new BufferedReader(new InputStreamReader(connec.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;
        try {
        	//dirty method ? must discard the HTML to parse JSON
            String json = output.split("\\[")[1];//take the part after the first { character, indicating JSON
            json = json.split("\\]")[0]; //cut after the end of the JSON
            json = "["+json+"]";
            json = json.replace("&quot;", "\"");
            json = json.replaceAll("<a href=.*</a>", "none");


            JSONArray j = new JSONArray(json);
            String[] dataset_names = new String[j.length()];
            for(int i=0;i<j.length();i++) {
            	dataset_names[i] = ((JSONObject) j.get(i)).getString("name");
            	sets.put(((JSONObject) j.get(i)).getString("name").toLowerCase(),((JSONObject)j.get(i)).getInt("id"));
            }

            
            
        }catch(Exception e){
        	System.out.println("ERROR : could not parse connection request result : "+e);
        }
        return sets;
		
	}
	
	/**
	 * Check if new dataset already exists for this user. true = continue, false = stop execution
	 * @param dataset name of dataset to verify
	 */
	public static boolean ConfirmIfDatasetExists(String dataset) {

		if(datasetToUpdate.isEditable()) {
			try {
				for(String d : GetDatasetsList()) {
					if(d.toLowerCase().equals(dataset.toLowerCase())) {
						IJ.error("Warning : a dataset named "+dataset+" already exists on your account. If you want to modify this dataset, please use the \"pick dataset\" button");

						return false;
					}
				}
			} catch (IOException e) {
				System.out.println("Error ConfirmIfDatasetExists : "+e);
			}
		}
		return true;
	}
		
	
	/**
	 * Adds an obj file to currently selected mesh with time point
	 * @param path path of the file to add
	 * @param t time step of the mesh
	 * @param quality quality of the mesh (needs to be 0-5)
	 * @param centerize option to center the mesh using the mesh gravity center
	 */
	public static void AddObjFileToDataset(String path, String t, String quality, boolean centerize) throws IOException {

		
		
		
		System.out.println("uploading "+path+" at time step : "+t);
		//we only deal with one file here
		//delete temp file if already exists
		File f = new File(LocalMNPath+"temp.bz2");
		if(f.exists() && !f.isDirectory()) {
			f.delete();
		}
		File dataFile = new File(path);
		
		//make sure the input file is not too big, otherwise return (300Mo max)
		if(dataFile.exists() && !dataFile.isDirectory()) {
			if((dataFile.length() / (1024*1024))>300) {
				IJ.error("Error while uploading file "+ path+" : file is too large (maximum of 300 megabytes");
				return;
			}
		}

		InputStream in = Files.newInputStream(dataFile.toPath());
		OutputStream fout = Files.newOutputStream(Paths.get(LocalMNPath+"temp.bz2"));
		BufferedOutputStream out = new BufferedOutputStream(fout);
		BZip2CompressorOutputStream bzOut = new BZip2CompressorOutputStream(out);

		byte[] data = Files.readAllBytes(dataFile.toPath());

		int n=0;
		while(-1!= (n = in.read(data))) {
			bzOut.write(data,0,n);
		}

		bzOut.close();
		in.close();

		String center="0.0,0.0,0.0";
		if(centerize) {
			center = GetObjFileCenter(path);
		}
		
		//get channel from file : 
		String channel="0";
		try {
			BufferedReader brTest = new BufferedReader(new FileReader(path));
		    String text = brTest .readLine();
		    channel = text.split(",")[2];
		    brTest.close();
		}catch(Exception e){
			IJ.error("could not find channel on your given mesh. subgroups must be of format : g t,id,channel. channel 0 is given as default");
		}
		
		
		//native java test : 

		Map<String,Object> params = new LinkedHashMap<>();
        params.put("uploadlargemesh",""+dataset_update_id);
        params.put("id_texture","-1");
        params.put("t",String.valueOf(t));
        params.put("quality",quality);
        params.put("channel",channel);
        if(centerize) {
        	params.put("center", center);
        }
        

        String charset = "UTF-8";
		File binaryFile = new File(LocalMNPath+"temp.bz2");
		String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
		String CRLF = "\r\n"; // Line separator required by multipart/form-data.
	
		URL url = new URL(morphonet_URL+"api/uploadlargemesh/");
		URLConnection connection = (URLConnection)url.openConnection();
	   
	    connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		connection.setRequestProperty("Authorization", "Token "+token);
		
		try (
			    OutputStream output = connection.getOutputStream();
			    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
			) {
			    // Send  param.
				 for (Map.Entry<String,Object> param : params.entrySet()) {
					 writer.append("--" + boundary).append(CRLF);
					 writer.append("Content-Disposition: form-data; name=\""+param.getKey()+"\"").append(CRLF);
					 writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
					 writer.append(CRLF).append(String.valueOf(param.getValue())).append(CRLF).flush();
				 }
			    // Send binary file.
			    writer.append("--" + boundary).append(CRLF);
			    writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
			    writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
			    writer.append("Content-Transfer-Encoding: binary").append(CRLF);
			    writer.append(CRLF).flush();
			    Files.copy(binaryFile.toPath(), output);
			    output.flush(); // Important before continuing with writer!
			    writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
		
			    // End of multipart/form-data.
			    writer.append("--" + boundary + "--").append(CRLF).flush();
			}
		
		// Request is lazily fired whenever you need to obtain information about response.
		
		Reader inR = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = inR.read()) >= 0;)
        	output+=(char)c;
        System.out.println("Mesh channel "+channel+", t"+t+", quality "+quality+" successfully uploaded !");
        
		
		
		if(f.exists() && !f.isDirectory()) {
			f.delete();
		}
	}
	
	/**
	 * Uploads Features from Fiji results table
	 * @param oid integer of the object id in the table (the ids column of the object)
	 * @param fid integer of the feature in the table (the feature column of the object you want to upload)
	 * @param time time step for the feature
	 * @param type type of the feature (string, float, selection, group, time)
	 * @param datasetName name of the dataset for which you upload the feature
	 * @param resName name of the feature table
	 */
	public static void UploadFeatureFromResults(int oid, int fid, String time, String type, String datasetName,String resName) throws IOException {
		
		ResultsTable tb=ResultsTable.getResultsTable(resName);
		String[] headings = tb.getHeadings();
		
		if(headings.length<=0) {
			IJ.error("Error: the Results array is empty. Please provide a valid Results array (refresh if needed)");
			return;
		}
		String objectID, featureID;
		try {
			objectID = headings[oid];
		}catch(Exception e) {
			IJ.error("The object ID input does not exist in the Results array. Please refresh and select a valid Object ID");
			return;
		}
		
		try {
			featureID = headings[fid];
		}catch(Exception e) {
			IJ.error("The Feature input does not exist in the Results array. Please refresh and select a valid Object ID");
			return;
		}
		
		//write a temp file with the infos from the results table (clear it first if it exists)
		File fout = new File(LocalMNPath+"temp.txt");
		if(fout.exists()) {
			fout.delete();
		}
		
		
		FileOutputStream fos = new FileOutputStream(fout);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
		
		bw.write("#Uploaded from ImageJ");
		bw.newLine();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  
		bw.write("#The "+dtf.format(now));
		bw.newLine();
		bw.write("#By "+login);
		bw.newLine();
		bw.write("#Dataset "+datasetName);
		bw.newLine();
		bw.write("type:"+type);
		bw.newLine();
	 
		
		for (int i = 0; i < tb.size(); i++) {
			bw.write(time+","+tb.getStringValue(objectID, i)+":"+tb.getStringValue(featureID, i));
			bw.newLine();
		}
		bw.close();
		
		//then, convert to .bz2
		File f = new File(LocalMNPath+"temp.bz2");
		if(f.exists() && !f.isDirectory()) {
			f.delete();
		}

		InputStream in = Files.newInputStream(fout.toPath());
		OutputStream foutb = Files.newOutputStream(Paths.get(LocalMNPath+"temp.bz2"));
		BufferedOutputStream out = new BufferedOutputStream(foutb);
		BZip2CompressorOutputStream bzOut = new BZip2CompressorOutputStream(out);

		byte[] data = Files.readAllBytes(fout.toPath());

		int n=0;
		while(-1!= (n = in.read(data))) {
			bzOut.write(data,0,n);
		}

		bzOut.close();
		in.close();
		
		//finally, put the request for uploading
		String dataType="2";
		if(type=="time" || type=="group" || type=="space") {
			dataType="1";
		}
		
		System.out.println("Uploading feature...");
		
		Map<String,Object> params = new LinkedHashMap<>();
        params.put("uploadlargecorrespondence",""+dataset_update_id);
        params.put("datatype",type);
        params.put("type",""+dataType);
        params.put("infos",""+featureID);// IMPORTANT : infos=name

        String charset = "UTF-8";
		File binaryFile = new File(LocalMNPath+"temp.bz2");
		String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
		String CRLF = "\r\n"; // Line separator required by multipart/form-data.
	
		URL url = new URL(morphonet_URL+"api/uploadinfoapi/");
		URLConnection connection = (URLConnection)url.openConnection();
	   
	    connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		connection.setRequestProperty("Authorization", "Token "+token);
		
		try (
			    OutputStream output = connection.getOutputStream();
			    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
			) {
			    // Send  param.
				 for (Map.Entry<String,Object> param : params.entrySet()) {
					 writer.append("--" + boundary).append(CRLF);
					 writer.append("Content-Disposition: form-data; name=\""+param.getKey()+"\"").append(CRLF);
					 writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
					 writer.append(CRLF).append(String.valueOf(param.getValue())).append(CRLF).flush();
				 }
			    // Send binary file.
			    writer.append("--" + boundary).append(CRLF);
			    writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
			    writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
			    writer.append("Content-Transfer-Encoding: binary").append(CRLF);
			    writer.append(CRLF).flush();
			    Files.copy(binaryFile.toPath(), output);
			    output.flush(); // Important before continuing with writer!
			    writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
		
			    // End of multipart/form-data.
			    writer.append("--" + boundary + "--").append(CRLF).flush();
			}
		
		// Request is lazily fired whenever you need to obtain information about response.

		
		Reader inR = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = inR.read()) >= 0;)
        	output+=(char)c;
        System.out.println(output);
        System.out.println("Features from Fiji for  t"+time+" successfully uploaded !");
        
		
        UploadFromFiji.setEnabled(true);
		
		if(f.exists() && !f.isDirectory()) {
			f.delete();
		}
		if(fout.exists()) {
			fout.delete();
		}
		
		
	}
	
	/**
	 * Upload Features from an already correctly formatted file
	 * @param path path of the file to upload
	 */
	public static void UploadFeatureFromFile(String path) throws FileNotFoundException, IOException {
		
		//then, convert to .bz2
		File f = new File(LocalMNPath+"temp.bz2");
		if(f.exists() && !f.isDirectory()) {
			f.delete();
		}
		File dataFile = new File(path);

		InputStream in = Files.newInputStream(dataFile.toPath());
		OutputStream foutb = Files.newOutputStream(Paths.get(LocalMNPath+"temp.bz2"));
		BufferedOutputStream out = new BufferedOutputStream(foutb);
		BZip2CompressorOutputStream bzOut = new BZip2CompressorOutputStream(out);

		byte[] data = Files.readAllBytes(dataFile.toPath());

		int n=0;
		while(-1!= (n = in.read(data))) {
			bzOut.write(data,0,n);
		}

		bzOut.close();
		in.close();
		
		//read the beginning of the file to get relevant info
		
		String name="";
		String type="";
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
		    String line;
		    int i=0;
		    while (i<50) {//check 20 first lines only, after that we consider it to be incorrect.
		       line = br.readLine();
		       if(line.toLowerCase().contains("#selection")) {
		    	   name=line.substring(11);
		       }
		       if(line.contains("type")) {
		    	   type=line.split(":")[1];
		       }
		       i++;
		    }
		}
		
		if(name=="") {
			IJ.error("Error: input feature file is incorrect : feature does not have a name (format : \"#Selection Name\")");
		}
		if(type=="") {
			IJ.error("Error: input feature file is incorrect : feature does not have a type (format : \"type:actual_type\")");
		}
		
		
		//finally, put the request for uploading
		String dataType="2";
		if(type=="time" || type=="group" || type=="space") {
			dataType="1";
		}
		
		
		Map<String,Object> params = new LinkedHashMap<>();
        params.put("uploadlargecorrespondence",""+dataset_update_id);
        params.put("datatype",type);
        params.put("type",""+dataType);
        params.put("infos",""+name);// IMPORTANT : infos=name

        String charset = "UTF-8";
		File binaryFile = new File(LocalMNPath+"temp.bz2");
		String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
		String CRLF = "\r\n"; // Line separator required by multipart/form-data.
	
		URL url = new URL(morphonet_URL+"api/uploadinfoapi/");
		URLConnection connection = (URLConnection)url.openConnection();
	   
	    connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		connection.setRequestProperty("Authorization", "Token "+token);
		
		try (
			    OutputStream output = connection.getOutputStream();
			    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
			) {
			    // Send  param.
				 for (Map.Entry<String,Object> param : params.entrySet()) {
					 writer.append("--" + boundary).append(CRLF);
					 writer.append("Content-Disposition: form-data; name=\""+param.getKey()+"\"").append(CRLF);
					 writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
					 writer.append(CRLF).append(String.valueOf(param.getValue())).append(CRLF).flush();
				 }
			    // Send binary file.
			    writer.append("--" + boundary).append(CRLF);
			    writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
			    writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
			    writer.append("Content-Transfer-Encoding: binary").append(CRLF);
			    writer.append(CRLF).flush();
			    Files.copy(binaryFile.toPath(), output);
			    output.flush(); // Important before continuing with writer!
			    writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
		
			    // End of multipart/form-data.
			    writer.append("--" + boundary + "--").append(CRLF).flush();
			}
		
		Reader inR = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = inR.read()) >= 0;)
        	output+=(char)c;
        System.out.println(output);
        System.out.println("Features from File "+path+" successfully uploaded !");
        
		
		
		if(f.exists() && !f.isDirectory()) {
			f.delete();
		}
				
		
	}
	
	
	/**
	 * takes the path to an obj file in input, returns the center of the mesh in a string of the format : "xValue,yValue,zValue"
	 * @param path path of the obj file
	 */
	public static String GetObjFileCenter(String path) throws IOException {
		
		FileInputStream fstream = new FileInputStream(path);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		
		String strLine;

		float x=0.0f,y=0.0f,z=0.0f;
		int nb=0;
		
		//Read File Line By Line
		while ((strLine = br.readLine()) != null)   {
			if(strLine.charAt(0)=='v' && strLine.charAt(1)!='n' && strLine.charAt(1)!='t') {
				String[] xyz = strLine.split(" ");
				if(xyz.length>=3) {
					x+=Float.valueOf(xyz[1]);
					y+=Float.valueOf(xyz[2]);
					z+=Float.valueOf(xyz[3]);
					nb++;
				}
			}
		}
		x/=nb;
		y/=nb;
		z/=nb;
		
		fstream.close();
		
		DecimalFormat df = new DecimalFormat("#.##");
		return ""+df.format(x).replace(',', '.')+","+df.format(y).replace(',', '.')+","+df.format(z).replace(',', '.');
	}
	

	/**
	 * adds an obj file to currently selected mesh with time point. Scale should match voxel size
	 * @param path path of the img file
	 * @param t time step of the image file
	 * @param imp ImagePlus object containing the image
	 * @param channel channel of the image on the dataset
	 * @param voxel_size voxel size of the image (in string format "x,y,z")
	 */
	public static void AddRawImageToDataset(String path, String t, float scale, ImagePlus imp, String channel, String voxel_size) throws IOException {

		//we only deal with one file here
		//delete temp file if already exists
		File f = new File(LocalMNPath+"temp.bz2");
		if(f.exists() && !f.isDirectory()) {
			f.delete();
		}
		File dataFile = new File(path);

		InputStream in = Files.newInputStream(dataFile.toPath());
		OutputStream fout = Files.newOutputStream(Paths.get(LocalMNPath+"temp.bz2"));
		BufferedOutputStream out = new BufferedOutputStream(fout);
		BZip2CompressorOutputStream bzOut = new BZip2CompressorOutputStream(out);

		byte[] data = Files.readAllBytes(dataFile.toPath());

		int n=0;
		while(-1!= (n = in.read(data))) {
			bzOut.write(data,0,n);
		}

		bzOut.close();
		in.close();

		
		Map<String,Object> params = new LinkedHashMap<>();
        params.put("uploadlargerawimages",""+dataset_update_id);
        params.put("t",String.valueOf(t));
        params.put("channel",""+channel);
        params.put("scale",""+scale);
        params.put("size", "("+imp.getWidth()+", "+imp.getHeight()+", "+imp.getNSlices()+")");
        params.put("voxel_size", voxel_size);

        String charset = "UTF-8";
		File binaryFile = new File(LocalMNPath+"temp.bz2");
		String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
		String CRLF = "\r\n"; // Line separator required by multipart/form-data.
	
		URL url = new URL(morphonet_URL+"api/uploadrawimageapi/");
		URLConnection connection = (URLConnection)url.openConnection();
	   
	    connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		connection.setRequestProperty("Authorization", "Token "+token);
		
		try (
			    OutputStream output = connection.getOutputStream();
			    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
			) {
			    // Send  param.
				 for (Map.Entry<String,Object> param : params.entrySet()) {
					 writer.append("--" + boundary).append(CRLF);
					 writer.append("Content-Disposition: form-data; name=\""+param.getKey()+"\"").append(CRLF);
					 writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
					 writer.append(CRLF).append(String.valueOf(param.getValue())).append(CRLF).flush();
				 }
			    // Send binary file.
			    writer.append("--" + boundary).append(CRLF);
			    writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
			    writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
			    writer.append("Content-Transfer-Encoding: binary").append(CRLF);
			    writer.append(CRLF).flush();
			    Files.copy(binaryFile.toPath(), output);
			    output.flush(); // Important before continuing with writer!
			    writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
		
			    // End of multipart/form-data.
			    writer.append("--" + boundary + "--").append(CRLF).flush();
			}
		
		// Request is lazily fired whenever you need to obtain information about response.
		
		Reader inR = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = inR.read()) >= 0;)
        	output+=(char)c;
        System.out.println("RAW IMAGE "+path+" for t"+t+" successfully uploaded !");
        
		
		
		if(f.exists() && !f.isDirectory()) {
			f.delete();
		}
	}
	
	
	/**
	 * clears raw image for current dataset in time step t. it uses the currently active dataset
	 * @param t time step to clear
	 * @param channel channel to clear
	 */
	public static void ClearRawImageFromDataset(String t, String channel) throws IOException{
		
		if(!connected) {
			IJ.error("ERROR : You are not connected !");
			return;
		}

		if(token.equals("")) {
			IJ.error("ERROR : Authentication credentials were not provided");
			return;
		}
		

		URL url = new URL(morphonet_URL+"api/deleterawimageapi/");

		String jsonPOSTData = "{\"deleterawimages\":\""+dataset_update_id+"\",\"t\":"+t+",\"channel\":"+channel+"}";

		byte[] postDataBytes = jsonPOSTData.getBytes();

		HttpsURLConnection connec = (HttpsURLConnection)url.openConnection();
		connec.setHostnameVerifier(DO_NOT_VERIFY);
		connec.setRequestMethod("POST");
		connec.setRequestProperty("Content-Type", "application/json");//attention to data type!
		connec.setRequestProperty("Authorization", "Token "+token);
		connec.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		connec.setDoOutput(true);

		connec.getOutputStream().write(postDataBytes);
		Reader in = new BufferedReader(new InputStreamReader(connec.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;
		JSONObject j = new JSONObject(output);
		System.out.println("ClearRawImageFromDataset : "+j.getString("result"));
		
	}
	
	/**
	 * clears data from currently selected dataset at time stamp t
	 * @param t time step to clear
	 * @param quality quality at which to clear
	 * @param channel channel to clear
	 */
	public static void ClearDatasetAtTimePoint(String t, String quality, String channel) throws IOException{
		
		if(!connected) {
			IJ.error("ERROR : You are not connected !");
			return;
		}

		if(token.equals("")) {
			IJ.error("ERROR : Authentication credentials were not provided");
			return;
		}
		
		System.out.println("Clearing dataset at time step "+t+", channel "+channel+", quality "+quality);

		URL url = new URL(morphonet_URL+"api/clearmeshapi/");

		String jsonPOSTData = "{\"clearmeshat\":\""+dataset_update_id+"\",\"t\":"+t+",\"quality\":"+quality+",\"channel\":"+channel+"}";

		byte[] postDataBytes = jsonPOSTData.getBytes();

		HttpsURLConnection connec = (HttpsURLConnection)url.openConnection();
		connec.setHostnameVerifier(DO_NOT_VERIFY);
		connec.setRequestMethod("POST");
		connec.setRequestProperty("Content-Type", "application/json");//attention to data type!
		connec.setRequestProperty("Authorization", "Token "+token);
		connec.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		connec.setDoOutput(true);

		connec.getOutputStream().write(postDataBytes);
		Reader in = new BufferedReader(new InputStreamReader(connec.getInputStream(), "UTF-8"));
        String output="";
        for (int c; (c = in.read()) >= 0;)
        	output+=(char)c;
		JSONObject j = new JSONObject(output);
		System.out.println(j.getString("result"));
		
	}
	
	/**
	 * returns lowest pixel value from image : we assume it is the value the background by default
	 * @param imp imagePlus to look at
	 */
	public static int GetLowestValueFromImage(ImagePlus imp) {
		
		if( imp == null ) { IJ.error( "You must select a currently opened image" ); return 0 ; }
		if (!imp.lock()) {System.out.println( "Your image is locked" ); return 0 ;}
		if ( imp.getNSlices()<=1) {IJ.error( "Your image should be in 3D " ); return 0 ;}
		if(imp.getType()!=ImagePlus.GRAY8 && imp.getType()!=ImagePlus.GRAY16 && imp.getType()!=ImagePlus.GRAY32) {IJ.error( "Your image is not in gray 8, 16 or 32bits. You must convert it before " ); return 0 ;}

		//check value of first pixel : we assume it is the background (looking at the whole image is too slow
		ImagePlus impR = imp.duplicate();
		
		
		ImageStatistics is = impR.getStatistics();
		imp.unlock();
		impR.changes=false;
		impR.close();
		return (int) is.histMin;
		
	}
	
	/**
	 * Opens default web navigator with URL to see the current dataset on Morphonet Website. Uses currently selected dataset
	 */
	public static void SeeDatasetOnMN() {
		try {
			String os = System.getProperty("os.name").toLowerCase();
			Runtime rt = Runtime.getRuntime();
			String url = morphonet_URL+"morphoapi?id_dataset="+dataset_update_id;
			if(os.indexOf("win") >= 0) {//windows
				
				rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
				
			}else if(os.indexOf("mac") >= 0) {//macOS
				
				rt.exec("open " + url);
				
			}else if(os.indexOf("nix") >=0 || os.indexOf("nux") >=0) {//Linux
				
				String[] browsers = { "google-chrome", "firefox", "mozilla", "epiphany", "konqueror",
				                                 "netscape", "opera", "links", "lynx" };
				StringBuffer cmd = new StringBuffer();
				for (int i = 0; i < browsers.length; i++)
				    if(i == 0)
				        cmd.append(String.format(    "%s \"%s\"", browsers[i], url));
				    else
				        cmd.append(String.format(" || %s \"%s\"", browsers[i], url)); 
				    // If the first didn't work, try the next browser and so on

				rt.exec(new String[] { "sh", "-c", cmd.toString() });
				
			}else {//how did you get here ?
				IJ.error("ERROR : OS unsupported to view dataset on Morphonet");
			}
		}catch(Exception e) {
			System.out.println("SeeDatasetOnMN ERROR : "+e);
		}
		
	}
	
	/**
	 * opens default web navigator with URL to create a morphoNet account
	 */
	public static void CreateMNAccount() {
		try {
			String os = System.getProperty("os.name").toLowerCase();
			Runtime rt = Runtime.getRuntime();
			String url = morphonet_URL+"signup";
			if(os.indexOf("win") >= 0) {//windows
				
				rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
				
			}else if(os.indexOf("mac") >= 0) {//macOS
				
				rt.exec("open " + url);
				
			}else if(os.indexOf("nix") >=0 || os.indexOf("nux") >=0) {//Linux
				
				String[] browsers = { "google-chrome", "firefox", "mozilla", "epiphany", "konqueror",
				                                 "netscape", "opera", "links", "lynx" };
				StringBuffer cmd = new StringBuffer();
				for (int i = 0; i < browsers.length; i++)
				    if(i == 0)
				        cmd.append(String.format(    "%s \"%s\"", browsers[i], url));
				    else
				        cmd.append(String.format(" || %s \"%s\"", browsers[i], url)); 
				    // If the first didn't work, try the next browser and so on

				rt.exec(new String[] { "sh", "-c", cmd.toString() });
				
			}else {//how did you get here ?
				IJ.error("ERROR : OS unsupported to view dataset on Morphonet");
			}
		}catch(Exception e) {
			System.out.println("SeeDatasetOnMN ERROR : "+e);
		}
		
	}
	
	/**
	 * Opens MorphoNet.org
	 */
	public static void GoToMorphoNet() {
		try {
			String os = System.getProperty("os.name").toLowerCase();
			Runtime rt = Runtime.getRuntime();
			String url = morphonet_URL;
			if(os.indexOf("win") >= 0) {//windows
				
				rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
				
			}else if(os.indexOf("mac") >= 0) {//macOS
				
				rt.exec("open " + url);
				
			}else if(os.indexOf("nix") >=0 || os.indexOf("nux") >=0) {//Linux
				
				String[] browsers = { "google-chrome", "firefox", "mozilla", "epiphany", "konqueror",
				                                 "netscape", "opera", "links", "lynx" };
				StringBuffer cmd = new StringBuffer();
				for (int i = 0; i < browsers.length; i++)
				    if(i == 0)
				        cmd.append(String.format(    "%s \"%s\"", browsers[i], url));
				    else
				        cmd.append(String.format(" || %s \"%s\"", browsers[i], url)); 
				    // If the first didn't work, try the next browser and so on

				rt.exec(new String[] { "sh", "-c", cmd.toString() });
				
			}else {//how did you get here ?
				IJ.error("ERROR : OS unsupported to view dataset on Morphonet");
			}
		}catch(Exception e) {
			System.out.println("SeeDatasetOnMN ERROR : "+e);
		}
		
	}
		
	/**
	 * save an imagePlus as byte array, containing only voxel values
	 * @param imp ImagePlus to convert and save
	 * @path path to save the image
	 */
	public static void SaveImagePlusAsByteArray(ImagePlus imp, String path) throws IOException {
		
		ImageStack ims = imp.getImageStack();
		int depth = imp.getNSlices();
		byte[] image;
		
		ByteBuffer buffer = ByteBuffer.allocate((ims.getHeight()*ims.getWidth()*depth)).order(ByteOrder.BIG_ENDIAN);
		
		for(int i=0;i<depth;i++) {
			for(int j=0;j<ims.getHeight();j++) {
				for(int k=0;k<ims.getWidth();k++) {
					buffer.put(((byte)ims.getVoxel(k, j, i)));
				}
			}
		}
		
		image = buffer.array();

		File outputFile = new File(path);
		try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
		    outputStream.write(image);
		} catch (IOException e) {
			System.out.println("could not write temp file for raw image uploading");
		}
		
	}
	
	
	/**
	 * Converts float array to byte array
	 * @param values float array to convert
	 */
	public static byte[] FloatArray2ByteArray(float[] values){
	    ByteBuffer buffer = ByteBuffer.allocate(4 * values.length).order(ByteOrder.BIG_ENDIAN);

	    for (float value : values){
	        buffer.putFloat(value);
	    }

	    return buffer.array();
	}
	

	/**
	 * Downscales an image without averaging values, using the nearest-neighbor value
	 * @param imp image to downscale
	 * @param scale_factor downscaling factor for axes X and Y
	 * @param scale_factor_z downscaling factor for axis Z
	 */
	public static ImagePlus DownScaleImagePlusNearestNeighbor(ImagePlus imp, int scale_factor, int scale_factor_z) {
		ImagePlus impR = Scaler.resize(imp, (int)Math.ceil(imp.getWidth()/scale_factor),  (int)Math.ceil(imp.getHeight()/scale_factor),  (int)Math.ceil(imp.getNSlices()/scale_factor_z), "none");
		 ImageStack imsB = imp.getImageStack();
		 ImageStack imsR = impR.getImageStack();


		 for(int i=0;i<imsR.getWidth();i++) {
			 for(int j=0;j<imsR.getHeight();j++) {
				 for(int k=0;k<imsR.getSize();k++) {//size or other ?
					 imsR.setVoxel(i, j, k, imsB.getVoxel(i*scale_factor, j*scale_factor, k*scale_factor_z));
				 }
			 }
		 }
		 impR.setStack(imsR);
		 return impR;
	}

	/**
	 * Adds a border of pixel_size pixels around the imageplus. only works with 3D images
	 * @param imp image to add a border to
	 * @param pixel_size size of the border in pixels
	 * @param border_value value to give to pixels of the border
	 */
	public static ImagePlus AddBorderToImagePlus(ImagePlus imp, int pixel_size,int border_value) {
		ImagePlus impR = Scaler.resize(imp, imp.getWidth()+(pixel_size*2), imp.getHeight()+(pixel_size*2), imp.getNSlices()+(pixel_size*2), "none");
		 //ImagePlus impR = imp.resize(imp.getWidth()/factor_scale, imp.getHeight()/factor_scale, imp.getNSlices()/factor_scale, "nearest");
		 ImageStack imsB = imp.getImageStack();
		 ImageStack imsR = impR.getImageStack();
		 for(int i=0;i<imsR.getWidth();i++) {
			 for(int j=0;j<imsR.getHeight();j++) {
				 for(int k=0;k<imsR.getSize();k++) {//size or other ?
					 if(i<pixel_size || j<pixel_size || k<pixel_size || i>=imsR.getWidth()-pixel_size || j>=imsR.getHeight()-pixel_size || k>=imsR.getSize()-pixel_size) {
						 imsR.setVoxel(i, j, k, border_value);
					 }else {
						 imsR.setVoxel(i, j, k, imsB.getVoxel(i-pixel_size, j-pixel_size, k-pixel_size));
					 }
				 }
			 }
		 }
		 impR.setStack(imsR);
		 return impR;
	}

	/**
	 * gets a 3D image from a 4d image
	 * @param imp image to get 3D frame
	 * @param nFrames number of frames in the 4D images
	 * @param frame frame to get
	 */
	public static ImagePlus getFrameFrom4DImagePlus(ImagePlus imp, int nFrames, int frame) {


		ImagePlus impR = IJ.createImage(imp.getTitle(), imp.getWidth(), imp.getHeight(), imp.getNSlices(), imp.getBitDepth());
		//used to be duplicate ^
		int frameLen = imp.getNSlices();
		ImageStack imsR = imp.getStack();
		int id_del=1;
		for(int i=1;i<=nFrames*frameLen;i++) {//stack starts at 1 for some reason
			if(i<(frame*frameLen) || i>(frame+1)*frameLen) {//delete all slice outside of wanted frame // >= ???
				imsR.deleteSlice(id_del);//always delete "currently deletable slice, to avoid out of range exception as the slice array changes in real-time
			}else {
				id_del++;
			}
		}
		impR.setStack(imsR);
		return impR;
	}

	
	/**
	 * Computes mesh from 3D image, and potentially uploads it to morphoNet directly
	 */
	public static void ComputeMeshesClassic(ImagePlus imp, String output,int startTS, int endTS, int downsampling_factor,int downsampling_factor_z, int background_threshold,
			float voxel_size_X, float voxel_size_Y, float voxel_size_Z, int min_pixel_elem, boolean border, boolean smooth,
			float smooth_passband, int smooth_iterations, boolean quadric_clustering,
			float nb_of_divisions, boolean decimate, float decimate_reduction, int verbose, boolean viewer, boolean uploadToo, String channel, int decim_threshold,
			boolean split_large, int split_threshold) {
		
		//make sure the output path exists
		if(!checkIfPathExists(output)) {
			IJ.error("3D image to mesh Conversion error : output path specified does not exists!");
		}
		
		

		String prefix="";
		int factor_scale = downsampling_factor;
		int min_pixel = min_pixel_elem;
		int smooth_angle = 120;
		float mesh_fineness = nb_of_divisions;


		int thread_amount = 2;//number of simultaneous threads



		if(verbose>=1) {
			System.out.println("converting "+imp.getTitle()+" to "+output);
		}

		//list of created files for upload
		List<String> createdObjs = new ArrayList<String>();


		//get currently selected image in Fiji


		if( imp == null ) { IJ.error( "You must select a currently opened image" ); return ; }
		if (!imp.lock()) {IJ.error( "Your image is locked" ); return ;}
		if ( imp.getNSlices()<=1) {IJ.error( "Your image should be in 3D " ); return ;}
		if(imp.getType()!=ImagePlus.GRAY8 && imp.getType()!=ImagePlus.GRAY16 && imp.getType()!=ImagePlus.GRAY32) {IJ.error( "Your image is not in gray 8, 16 or 32bits. You must convert it before " ); return ;}

		if(convertBtn!=null) {
			convertBtn.setEnabled(false);
		}
		
		

		final int nSlices = imp.getNSlices();
        int nFrames = imp.getNFrames();




		//temp dialog for parameters

        if(nFrames>1) {

			startF = startTS+1;
			endF = endTS+1;

		}else { startF=1; endF=1; }



		int threshold = background_threshold;
		factor_scale = downsampling_factor;

		thread_amount = Runtime.getRuntime().availableProcessors()*2;
					

		//First off, rescale the image according to scale factor
		//divide the image by resampling factor
		
		if(downsampling_factor_z!=downsampling_factor) {//if factor is different, scale voxel_size z
			voxel_size_Z = voxel_size_Z/ ((float)downsampling_factor/(float)downsampling_factor_z);
		}


		//if the image is in 4 dimensions, we need to cut it in several ImagePlus elements, which we will then iterate on
		for(int frame=startF-1;frame<endF;frame++) {
			if(verbose==2)
				System.out.println("treating time point number "+(frame+1));

			//create viewer window
			Image3DUniverse univ = new Image3DUniverse(10,10);
			if(viewer) {
				univ = new Image3DUniverse(500,500);
				univ.show();
			}



			ImagePlus impB = imp.duplicate();

			ImagePlus impD;
			if(nFrames>1) {
				impD = getFrameFrom4DImagePlus(impB,nFrames, frame);
			}else {
				impD = impB.duplicate();
			}



			if(factor_scale>1 || downsampling_factor_z>1) {
				impD = DownScaleImagePlusNearestNeighbor(impD,factor_scale,downsampling_factor_z);//used to be imp
			}

			
			

			ImagePlus impR = impD.duplicate();

			//case border : make the image bigger
			if(border) {
				impR = AddBorderToImagePlus(impD,2,threshold);
			}



	        int nx = impR.getWidth();
	        int ny = impR.getHeight();
	        int nz = impR.getNSlices();

	        //max nb of threads is depth of 3D image (useless code ?)
	        if(thread_amount>nz)
	        	thread_amount=nz;

	        int shift_face=1;




			//create a streamwriter to avoid handling large strings
			 FileOutputStream objfile;
			 OutputStreamWriter out;
			 


			try {

					//ATTENTION : BEFORE THRESHOLD WAS >, NOW IT IS !=
				
					int c=1;
					List<Integer> listIDS=new ArrayList<Integer>();
					for ( int z = 1; z <= nSlices; ++z )
					{
						final int index = impR.getStackIndex( c, z, frame );
						final ImageProcessor ip = impR.getStack().getProcessor( index );

						if(impR.getType()==ImagePlus.GRAY8) { //8 Bits
							ByteProcessor bp=ip.convertToByteProcessor(false);
							float[] pixels = (float[])bp.getPixels();
							for (int x = 0; x <pixels.length; x++) {
								int v=(int)pixels[x];
								if(v!=threshold && !listIDS.contains(v)) listIDS.add(v);
							}
						}
						else if(impR.getType()==ImagePlus.GRAY16) { //16 Bits
							FloatProcessor bp=ip.convertToFloatProcessor();
							float[] pixels = (float[])bp.getPixels();
							for (int x = 0; x <pixels.length; x++) {
								int v=(int)pixels[x];
								if(v!=threshold && !listIDS.contains(v)) listIDS.add(v);
							}
						}
						else if(impR.getType()==ImagePlus.GRAY32) { 	//32 Bits
							FloatProcessor bp=ip.convertToFloatProcessor();
							float[] pixels = (float[])bp.getPixels();
							for (int x = 0; x <pixels.length; x++) {
								int v=(int)pixels[x];
								if(v!=threshold && !listIDS.contains(v)) listIDS.add(v);
							}
						}
					}
					 if(listIDS.size()>MAXOBJ) {
						 IJ.error("Cannot upload, This dataset contains more than "+MAXOBJ+" objects");
						 return;
					 }
					 if(listIDS.size()==0) {
						 IJ.error("We didn't find any objects");
						 return;
					 }
					 //sort the collection in order
					 Collections.sort(listIDS);

					
					 
					 //then, for each index, we convert in meshes


					 //create parameter variable for cleaner thread params passing
					 ConvertParameters params = new ConvertParameters(prefix, factor_scale,background_threshold,nx,ny,nz,
							 voxel_size_X,voxel_size_Y,voxel_size_Z,min_pixel,smooth,smooth_angle,
							 smooth_passband,smooth_iterations,quadric_clustering,mesh_fineness,decimate,decimate_reduction,verbose,decim_threshold);



					 //input
					 ImageStack ims = impR.getImageStack();


					 //thread array
					 ImageToMeshProcessor[] threads = new ImageToMeshProcessor[listIDS.size()];

					 //output array
					 vtkPolyData[] pdatas = new vtkPolyData[listIDS.size()];
					 for(int i=0;i<pdatas.length;i++) {
						 pdatas[i] = new vtkPolyData();
					 }

					 //manage convert progress bar : 
					 int progressValue=0;
					 if(ConvertProgress!=null) {
						 ConvertProgress.setVisible(true);
						 ConvertProgress.setValue(progressValue);
						 ConvertProgress.setMinimum(0);
						 ConvertProgress.setMaximum(listIDS.size()+5);
					 }
					 
					 
					 for(int i=0;i<listIDS.size();i+=thread_amount) {

						 //if we are going above the max amount of elements, adapt thread_amount for the last iteration
						 if(i+thread_amount>listIDS.size())
							 thread_amount = listIDS.size() - i;

						 //launch the threads, then join them (wait for them to be over to go on), then you get all the results
						 //ATTENTION peut-être mal compris ici ?
						 for(int j=0;j<thread_amount;j++) {
							threads[i+j] = new ImageToMeshProcessor(listIDS.get(i+j),ims,params);
							threads[i+j].start();

						 }
						 for(int j=0;j<thread_amount;j++) {
							threads[i+j].join();
							progressValue++;
							ConvertProgress.setValue(progressValue);

						 }
						 for(int j=0;j<thread_amount;j++) {
							pdatas[i+j] = threads[i+j].getGeneratedMesh();
							if(viewer)
								univ.addCustomMesh(convertPolyDataToMesh(pdatas[i+j]), "s_mesh"+(i+j));
						 }

					 }
					 
					 int vert_threshold = split_threshold;
					 int iter=1;
					 int pt=0;
					 
					 //create a streamwriter to avoid handling large strings
					 //must edit output name to add frame number before .obj
					 int frameID=frame;
					 if(frame==0 && endTS==-1)//special case if we are at the first frame, and end time stop is at -1 (stack case), replace frame with the selected time stamp
						 frameID=startTS;
					 String frameOutput = formatOutputFile(output, frameID);

					 
					 
					 objfile = new FileOutputStream(frameOutput);
					 out = new OutputStreamWriter(objfile);

					 //add to created for upload later
					 createdObjs.add(frameOutput);
					 

					 //finally, write the contents of the meshes to the file :
					 int nb=0;
					 int size=pdatas.length;
					 int pcount=0;
					 for(vtkPolyData p : pdatas) {
						 if(p==null) {
							 System.out.println("elem "+frameID+" NULL");
						 }else {
							 if(p.GetPoints()==null) {
								 System.out.println("elem "+frameID+".GetPoints() NULL");
							 }
						 }
						 
						 if(p.GetPoints()!=null) {
							 if(p.GetPoints().GetNumberOfPoints()>0) {
								 out.write("g "+(frameID)+","+listIDS.get(nb)+","+channel+"\n");// needs to look like : g t,id[,ch], update frame with time_stamp choice
								 if(verbose>=2) {
									 System.out.println("---> writing elem "+(frameID)+","+listIDS.get(nb)+" to file "+frameOutput);
								 }

								 for(int i=0;i<p.GetPoints().GetNumberOfPoints();i++) {
									 double[] v = p.GetPoints().GetPoint(i);
									 pt++;
									 out.write("v "+(v[0])*(float)factor_scale+" "+(v[1])*(float)factor_scale+" "+(v[2])*(float)factor_scale+"\n");
								 }
								 for(int i=0;i<p.GetNumberOfCells();i++) {
									 out.write("f "+(shift_face+p.GetCell(i).GetPointIds().GetId(0))+ " "+(shift_face+p.GetCell(i).GetPointIds().GetId(1))+ " "+(shift_face+p.GetCell(i).GetPointIds().GetId(2))+ "\n");
								 }
								 shift_face+=p.GetPoints().GetNumberOfPoints();
							 }
							 
						 }else {
							 System.out.println("skip elem "+listIDS.get(nb)+" at WRITING step !");
						 }
						 nb++;
						 
						 if(split_large && pt>vert_threshold && (pcount<size-1)) {
							 out.flush();
							 out.close();
							 
							 String newOut = output.substring(0, output.length()-5)+iter+".obj";
							 String newFrameOutput = formatOutputFile(newOut, frameID);
							 							 
							 objfile = new FileOutputStream(newFrameOutput);
							 out = new OutputStreamWriter(objfile);

							 //add to created for upload later
							 replaceOff=true;
							 createdObjs.add(newFrameOutput);
							 shift_face=1;
							 pt=0;
							 iter++;
						 }
						 pcount++;
					 }
					 ConvertProgress.setValue(ConvertProgress.getMaximum());


					 out.flush();
					 out.close();
					 objfile.flush();
					 objfile.close();
					 
					 
			}catch(Exception e1) {
				System.out.println("error somewhere : "+e1);
				e1.printStackTrace();
				IJ.error( "error somewhere : "+e1 );
				
			}
			impB.changes=false;
			impB.close();
			impD.changes=false;
			impD.close();
			impR.changes=false;
			impR.close();

		}


        imp.unlock();

        if(uploadToo) {
        	if(create_dataset) {//only create dataset if you did not pick one !
				if(!datasetToUpdate.getText().equals("")) {
					try {
						System.out.println("CREATING DATASET : "+datasetToUpdate.getText());
						CreateDataset(datasetToUpdate.getText());
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			if(dataset_update_id!=-1) {//only update if we have a valid dataset_id, either created or picked !
				try {
					//check if uploading a file or an entire directory
					File uploadpath = new File(pathToUploadTF.getText());
					if(uploadpath.exists()) {
						if(uploadpath.isDirectory()) {//dir case
							
							
							
							//manage progressbar
							UploadProgress.setVisible(true);
							UploadProgress.setMinimum(0);
							UploadProgress.setMaximum(createdObjs.size());
							UploadProgress.setValue(0);
							int uploadProgressValue=0;
							boolean replaceMany = false;
							
							for(String s : createdObjs) {
								if(datasetClear.getState() && !replaceMany)
									ClearDatasetAtTimePoint(GetTimeStepFromMeshName(s),String.valueOf(qualityTF.getValue()),channelTF.getText());
								AddObjFileToDataset(s,GetTimeStepFromMeshName(s),String.valueOf(qualityTF.getValue()),centerB.getState());
								uploadProgressValue++;
								UploadProgress.setValue(uploadProgressValue);
								if(replaceOff)
									replaceMany=true;
							}
						}else {//file case
							if(datasetClear.getState())
								ClearDatasetAtTimePoint(GetTimeStepFromMeshName(pathToUploadTF.getText()),String.valueOf(qualityTF.getValue()),channelTF.getText());
							AddObjFileToDataset(pathToUploadTF.getText(),GetTimeStepFromMeshName(pathToUploadTF.getText()),String.valueOf(qualityTF.getValue()),centerB.getState());
						}
					}
				}catch(Exception e) {
					IJ.error("error during file upload !"+e );
				}
			}
        }
        
        if(convertBtn!=null) {
			convertBtn.setEnabled(true);
		}
        if(seeDatasetInMN!=null) {
        	seeDatasetInMN.setVisible(true);
        }
        
        replaceOff=false;
	}




	/**
	 * Builds parameters for web Requests
	 * @param params parameters of the request
	 */
	public static byte[] buildURLParams(Map<String,Object> params) throws UnsupportedEncodingException {
		//System.out.println("Build Params");
		StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        return postDataBytes;

	}

	/**
	 * checks the presence of .obj files in path
	 * @param path path to check
	 */
	public static int checkObjMeshesInPath(String path) throws IOException{
		//if the path contains an obj file at the end, clean it
		String cleanPath="";
		String[] pp = path.split("\\"+File.separator);
		for(String d : pp) {
			if(!d.contains(".obj"))
				cleanPath = cleanPath + d +File.separator;
		}

		if(!new File(cleanPath).isDirectory()) {
			//throw new IllegalArgumentException("path must be a directory !");
			return 0;
		}

		List<String> res;
		try (Stream<Path> walk = Files.walk(new File(cleanPath).toPath(),1)) {
			res = walk
                    .filter(p -> !Files.isDirectory(p))
                    // this is a path, not string,
                    // this only test if path end with a certain path
                    //.filter(p -> p.endsWith(".obj"))
                    // convert path to string first
                    .map(p -> p.toString().toLowerCase())
                    .filter(f -> f.endsWith(".obj"))
                    .collect(Collectors.toList());
        }

		return res.size();
	}

	/**
	 * checks if path (can be path to a file) exists
	 * @param path path to check
	 */
	public static boolean checkIfPathExists(String path) {
		String cleanPath="";
		String[] pp = path.split("\\"+File.separator);
		for(String d : pp) {
			if(!d.contains(".obj"))
				cleanPath = cleanPath + d +File.separator;
		}

		if(!new File(cleanPath).isDirectory() || !new File(cleanPath).exists()) {
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * checks the presence of .obj files in path
	 * @param path path to check
	 */
	public static List<String> getObjMeshesInPath(String path) throws IOException{
		//if the path contains an obj file at the end, clean it
		String cleanPath="";
		String[] pp = path.split("\\"+File.separator);
		for(String d : pp) {
			if(!d.contains(".obj"))
				cleanPath = cleanPath + d +File.separator;
		}

		if(!new File(cleanPath).isDirectory()) {
			throw new IllegalArgumentException("path must be a directory ! is currently : "+cleanPath);
		}

		List<String> res;
		try (Stream<Path> walk = Files.walk(new File(cleanPath).toPath(),1)) {
			res = walk
                    .filter(p -> !Files.isDirectory(p))
                    // this is a path, not string,
                    // this only test if path end with a certain path
                    //.filter(p -> p.endsWith(".obj"))
                    // convert path to string first
                    .map(p -> p.toString())//.toLowerCase())
                    .filter(f -> f.endsWith(".obj"))
                    .collect(Collectors.toList());
        }

		return res;
	}

	/**
	 * remove the .obj file from a path. example : path/to/mesh.obj -> path/to/
	 * @param path path of file
	 */
	public static String removeFileFromPath(String path) {
		String cleanPath="";
		String[] pp = path.split("\\"+File.separator);
		for(String d : pp) {
			if(!d.contains(".obj"))
				cleanPath = cleanPath + d +File.separator;
		}
		return cleanPath;
	}
	
	/**
	 * Formats the output file properly. For instance, change path/file.obj in path/filei.obj
	 * @param path path of file
	 * @param i time step to add to file name
	 */
	public static String formatOutputFile(String path, int i){
	
		String[] pp = path.split("\\.");
		String ret = "";
		if(pp.length>2) {//case for . in path
			for(int j=0;j<pp.length-1;j++) {
				if(j>0)
					ret = ret + ".";
				ret = ret+pp[j];
			}
			ret = ret +"_t"+ i + "."+pp[pp.length-1];
		}else {
			ret = path.split("\\.")[0]+"_t"+i+"."+path.split("\\.")[1];
		}
	
		return ret;
	
	}
	
	/**
	 * Gets filename from path. For instance, path/to/file.obj returns file.obj
	 * @param path path of file
	 */
	public static String getFileFromPath(String path) {
		return path.split("\\"+File.separator)[path.split("\\"+File.separator).length-1];
	}
	
	/**
	 * remove the extension from a string : turn "file.extension" into "file"
	 * @param filename name of the file
	 */
	public static String removeExtensionFromFile(String filename) {
		String[] tokens = filename.split("\\.");
		String ret="";
		for(int i=0;i<tokens.length-1;i++) {
			if(i>0)
				ret+=".";
			ret+=tokens[i];
		}
		return ret;
	}
	
	/**
	 * Converts vtk polyData format to CustomMesh
	 * @param pd vtkPolyData to convert
	 */
	public static CustomMesh convertPolyDataToMesh(vtkPolyData pd) {
		List<Point3f> verts = new ArrayList<Point3f>();
	

		for(int i=0;i<pd.GetNumberOfCells();i++) {
			//must add points in batches of 3 (3 vertices = one triangle)
			Point3f p1 = new Point3f((float)pd.GetPoints().GetPoint((pd.GetCell(i).GetPointIds().GetId(0)))[0],
					(float)pd.GetPoints().GetPoint((pd.GetCell(i).GetPointIds().GetId(0)))[1],
					(float)pd.GetPoints().GetPoint((pd.GetCell(i).GetPointIds().GetId(0)))[2] );
	
			Point3f p2 = new Point3f((float)pd.GetPoints().GetPoint((pd.GetCell(i).GetPointIds().GetId(1)))[0],
					(float)pd.GetPoints().GetPoint((pd.GetCell(i).GetPointIds().GetId(1)))[1],
					(float)pd.GetPoints().GetPoint((pd.GetCell(i).GetPointIds().GetId(1)))[2] );
	
			Point3f p3 = new Point3f((float)pd.GetPoints().GetPoint((pd.GetCell(i).GetPointIds().GetId(2)))[0],
					(float)pd.GetPoints().GetPoint((pd.GetCell(i).GetPointIds().GetId(2)))[1],
					(float)pd.GetPoints().GetPoint((pd.GetCell(i).GetPointIds().GetId(2)))[2] );
			verts.add(p1);
			verts.add(p2);
			verts.add(p3);
		}
	
	
		CustomMesh c = new CustomTriangleMesh(verts);
		return c;
	}


}
