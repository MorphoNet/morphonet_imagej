package com.MorphoNet.imagej;

import java.awt.TextField;

import ij.gui.GenericDialog;

public class PasswordDialog extends GenericDialog{

	public PasswordDialog(String title) {
		super(title);
	}
	
	public void addPasswordField(String label, String defaut,int size) { 
		addStringField(label,defaut,size); 
		TextField tf = (TextField)stringField.elementAt(stringField.size()-1); 
		tf.setEchoChar('*'); 
	} 
}
