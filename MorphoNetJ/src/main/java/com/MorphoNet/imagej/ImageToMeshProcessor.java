package com.MorphoNet.imagej;


import ij.ImageStack;
import vtk.vtkDecimatePro;
import vtk.vtkImageData;
import vtk.vtkMarchingCubes;
import vtk.vtkPolyData;
import vtk.vtkQuadricClustering;
import vtk.vtkTIFFWriter;
import vtk.vtkWindowedSincPolyDataFilter;

public class ImageToMeshProcessor implements Runnable{

	
	private static int VTK_UNSIGNED_CHAR = 3;
	
	
	private Thread t;
	private vtkPolyData pdata;
	
	private ImageStack inputImage;
	private ConvertParameters params;
	private int elem;
	
	ImageToMeshProcessor(int elemNb, ImageStack ims, ConvertParameters p){
		inputImage = ims;
		params = p;
		elem = elemNb;
	}
	
	
	
	@Override
	public void run() {
		
		 vtkImageData newData = new vtkImageData();
		 newData.SetDimensions(params.nx, params.ny, params.nz);
		 newData.SetOrigin(0, 0, 0);
		 newData.SetSpacing(params.voxel_size_x,params.voxel_size_y,params.voxel_size_z);
		 newData.AllocateScalars(VTK_UNSIGNED_CHAR, 1);
		 newData.GetPointData().GetScalars().Fill(0);
		 
		 final int[] dims = newData.GetDimensions();
		 
		 int pxElem=0;
		 ImageStack is = inputImage;
		 for (int z=0; z<dims[2]; z++) {
		    for (int y=0; y<dims[1]; y++) {
			  for (int x=0; x<dims[0]; x++) {
				  if(is.getVoxel(x, y, z)==elem) {
						 newData.SetScalarComponentFromDouble(x, y, z, 0, 255 );
						 pxElem++;
				 }
				 
			  }
		    }
		 }
		 

		 
		 //test minvoxel : if number of voxels is too low : return empty polyData
		 if(pxElem<params.min_pixel) {
			 if(params.verbose>=2) {
				 System.out.println("---> Skipping "+params.prefix+elem+ " Because contains less than "+params.min_pixel+" voxels");
			 }
			 this.pdata = new vtkPolyData();
			 return;
		 }
		 
		 
		 //marching cubes
		 vtkMarchingCubes contour = new vtkMarchingCubes();
		 
		 contour.SetInputData(newData);
		 contour.ComputeNormalsOn();
		 contour.ComputeGradientsOn();
		 contour.SetValue(0, 205);//used to be 255, since 205 is default value we gain immense time during newData creation
		 contour.Update();
		 
		 newData.Delete();
		 
		 vtkPolyData pdata = contour.GetOutput();
		 
		 
		 
		 boolean skip = false;
		 if(pdata.GetPoints() == null) {
			 skip=true;
			 System.out.println("skip elem "+elem+" at step contour !");
		 }
		 if(!skip && params.smooth) {
			 vtkWindowedSincPolyDataFilter smoother = new vtkWindowedSincPolyDataFilter();
			 smoother.SetInputData(pdata);
			 smoother.SetFeatureAngle(params.smooth_angle);
			 smoother.SetPassBand(params.smooth_passband);
			 smoother.SetNumberOfIterations(params.smooth_iterations);
			 smoother.NonManifoldSmoothingOn();
			 smoother.NormalizeCoordinatesOn();
			 smoother.Update();
			 if(smoother.GetOutput()!=null) {
				 if(smoother.GetOutput().GetPoints()!=null) {
					 if(smoother.GetOutput().GetPoints().GetNumberOfPoints()>0)
						 pdata = smoother.GetOutput();
				 }
			 }
			 else
				 skip=true;
			 smoother.Delete();
		 }
		 
		 
		 if(!skip && params.quadric_clustering) {
			 vtkQuadricClustering dec = new vtkQuadricClustering();
			 dec.SetInputData(pdata);
			 dec.SetNumberOfDivisions((int)(params.mesh_fineness*(float)params.nx/2f),(int)(params.mesh_fineness*(float)params.ny/2f),(int)(params.mesh_fineness*(float)params.nz/2f)); 
			 dec.SetFeaturePointsAngle(30.0f);
			 dec.CopyCellDataOn();
			 dec.Update();
			 if(dec.GetOutput()!=null) {
				 if(dec.GetOutput().GetPoints()!=null) {
					 if(dec.GetOutput().GetPoints().GetNumberOfPoints()>0)
						 pdata = dec.GetOutput();
				 }
			 }
			 else
				 skip=true;
			 dec.Delete();
		 }

		 
		 //
		 int seuil=params.decim_threshold;
		 vtkPolyData pdatacp = new vtkPolyData();
		 float dec_red=params.decimate_reduction;
		 int nbPoints=0;
		 
		 if(!skip && params.decimate) {
		 
			 do {
				 nbPoints=0;
				 
					 vtkDecimatePro decimate_pro = new vtkDecimatePro();
					 decimate_pro.SetInputData(pdata);
					 decimate_pro.SetTargetReduction(dec_red);
					 decimate_pro.Update();
					 if(decimate_pro.GetOutput()!=null) {
						 if(decimate_pro.GetOutput().GetPoints()!=null) {
							 if(decimate_pro.GetOutput().GetPoints().GetNumberOfPoints()>0) {
								 pdatacp = decimate_pro.GetOutput();
								 nbPoints = (int) pdatacp.GetPoints().GetNumberOfPoints();
							 }
						 }
					 }
					 else
						 skip=true;
					 decimate_pro.Delete();
					 dec_red-=0.05f;
				 
	
			 }while(pdatacp!=null && nbPoints<seuil && !skip && dec_red>0f);
		 
		 }
		 
		 if(dec_red>0f && pdatacp!=null && pdatacp.GetPoints()!=null && pdatacp.GetPoints().GetNumberOfPoints()>0)
			 pdata = pdatacp;
		 

		 
		 
		 //we do not write the obj data in the  thread, as we need to keep the shift_face in order for a correct file creation
		 //(we need to know the amount of faces for each of the previous elements, maybe this will change if it is faster and
		 //we can make it work without conflicts
		 if(!skip) {
			 if(params.verbose>=2) {
				 System.out.println("---> Create "+params.prefix+elem+ "(with "+pxElem+" voxels converted in "+pdata.GetPoints().GetNumberOfPoints()+" vertex");
			 }
		 }
		 
		 this.pdata = new vtkPolyData();
		 this.pdata=pdata; //at this point the final pdata is set, ready to be recuperated in "main thread"
		 
		 contour.Delete();
		
		
		 
	}
	
	
	public vtkPolyData getGeneratedMesh() {
		return pdata;
	}
	
	public void join() {
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void start() {
		
		if(t==null) {
			t = new Thread(this,"t"+elem);
			t.start();
		}
		
	}

}
