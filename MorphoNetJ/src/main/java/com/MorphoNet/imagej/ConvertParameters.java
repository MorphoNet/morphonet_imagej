package com.MorphoNet.imagej;

public class ConvertParameters {

	public String prefix;
	public int factor_scale;
	public int background_threshold;
	public float voxel_size_x;
	public float voxel_size_y;
	public float voxel_size_z;
	public int nx;
	public int ny;
	public int nz;
	public int min_pixel;
	public boolean smooth;
	public int smooth_angle;
	public float smooth_passband;
	public int smooth_iterations;
	public boolean quadric_clustering;
	public float mesh_fineness;
	public boolean decimate;
	public float decimate_reduction;
	public int verbose;
	public int decim_threshold;
	
	public ConvertParameters(String prefix, int factor_scale, int background_threshold, int nx,int ny,int nz, float voxel_size_x,
			float voxel_size_y, float voxel_size_z, int min_pixel, boolean smooth,
			int smooth_angle, float smooth_passband,int smooth_iterations, boolean quadric_clustering, float mesh_fineness, boolean decimate,
			float decimate_reduction, int verbose, int decim_threshold) {
		this.prefix=prefix;
		this.factor_scale=factor_scale;
		this.background_threshold=background_threshold;
		this.nx=nx;
		this.ny=ny;
		this.nz=nz;
		this.voxel_size_x = voxel_size_x;
		this.voxel_size_y = voxel_size_y;
		this.voxel_size_z = voxel_size_z;
		this.min_pixel=min_pixel;
		this.smooth=smooth;
		this.smooth_angle=smooth_angle;
		this.smooth_passband=smooth_passband;
		this.smooth_iterations=smooth_iterations;
		this.quadric_clustering=quadric_clustering;
		this.mesh_fineness=mesh_fineness;
		this.decimate=decimate;
		this.decimate_reduction=decimate_reduction;
		this.verbose=verbose;
		this.decim_threshold = decim_threshold;
		
	}
}
